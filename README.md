# DeskMap

DeskMap is a map browser desktop version such as Google Maps.

Its goal is to allow its users to find an itinerary in any city around the world and also provide them with complementary information. 

This software was developed as an assignment during the 2019/20 academic year at the CERI, which is part of the Avignon University (France), by the following students:
* CHAIBAINOU Caroline
* PARRILLA Valentin
* POTIN Clement
* SASSI Nader

This software use OpenStreetMap databases to do its mapping an calculations, and is developed in Java with the use of JavaFX for the interface.


## Organization

The entirety of the software was developed in the "fr.univavignon.ceri.deskmap" package.

The source code features :
* A requester (OSMData) which send request using the Overpass Interpreter to get XML data from OSM;
* A parser (Parser) which transform its data into variables usable by our software;
* A converter (Converter) which adapts this variables to our software by using mathematical calculations;
* A drawer (Pencil) which draws the map into the interface;
* An interface (DeskMapInterface) which connects the user to the software by providing a form in the left panel, and the map in the right one;
* A launcher (Launcher) which launches the whole application.

Minor classes also features an A* algorithm for the software to find the shortest path from a point to another, an autocompletion class to facilitate the user searches, and classes which structures our data.


## Installation

There is currently no "jar" or "exe" file to launch directly the program from.

As the software is still in development we only provide a version which requires an IDE, such as Eclipse (free) or IntelliJ IDEA (free for students), to run on.

If you're interested in seeing the current development state, follow the next indications to install it properly and make sure it works :
1. Import the project into your IDE;
2. Make sure you use at least the version 8 of Java;
3. Also make sure that JavaFX is installed and your project is configured to use it (check https://download.java.net/general/javafx/eclipse/tutorial.html for more information about JavaFX installation);
4. Run the Launcher by right clicking on the "Launcher" class and select "Run 'Launcher'" (no program argument needed).

The software should start properly.


## Use

After you started the software for the first time, it will generate a "Cities.csv" file containing important information about the French cities (the only one on which the software can run for the moment). It might freeze the interface a bit, but once the file is created you'll no longer be disturbed, even after restarted your computer.

Once it's done, the interface is pretty intuitive :
* The left panel is a form. Once you typed a valid city in the first search bar and pressed "OK", the application will start making it's job to find the data it need (therefore, an internet connexion is needed, unless you already searched for this city before), do the calculations and voila : the map should be drawn. The interface is still frozen during this process at the current software's stage, but we are working on it. If you want to make sure things are happening, check you IDE's console, messages should be regularly printed to inform the user about what the software is currently doing;
* After the map is loaded, you can complete the form by filling in the departure and arrival address fields, which will allow you to search for the shortest route between the two points. Be aware that this feature is still in its alpha version;
* On the right panel, you can see the map of the currently loaded city. The moving features are intentionally locked on the minimum zoom possible so you don't move to far away from the city, as only a square around the city center point is drawn;
* You can zoom in and out by using the bottom-right "+" and "-" buttons, selecting the scroll bar or simply using the mouse wheel;
* You can also move around by using the top-right arrows, or simply clicking and dragging the map with your mouse. Note that you need to release your mouse's left button for the map to be refreshed;
* As you zoom in, you will notice that buildings structures appear;
* At a certain zoom level, points of interest are also shown by little icons appearing when your mouse pass close enough (schools, hospitals, police-related buildings, town hall, ...). You can click on them to show information about these points.


## Dependencies

The project relies on the following items :
* A working and updated Java IDE;
* Java 8 or higher;
* The JavaFX version corresponding to the current version of Java you are using.

## References

During development, we use the following bibliographic resources :

![Image from : Captures/References.png](Captures/References.png)

