package fr.univavignon.ceri.deskmap;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;


/**
 * @author      Clément Potin <clement.potin@alumni.univ-avignon.fr>
 * @version     2.0
 */
class OSMData {

    /** Overpass interpreter URL */
//    static final String OVERPASS_URL = "https://overpass-api.de/api/interpreter?data=";
    static final String OVERPASS_URL = "https://overpass.kumi.systems/api/interpreter?data=";

    /** Directory in which xml files will be stored */
    static final String DIRNAME = "OSMData";

    /** South outer limit of the city (min latitude) */
    static double citySouthLimit;

    /** North outer limit of the city (max latitude) */
    static double cityNorthLimit;

    /** West outer limit of the city (min longitude) */
    static double cityWestLimit;

    /** East outer limit of the city (max longitude) */
    static double cityEastLimit;

    /** Square radius used to set the delimitation of the zone in which to search for data */
    private static final double SQUARE_RADIUS = 0.05;

    /** Delimitation of the city (citySouthLimit, cityWestLimit, cityNorthLimit, cityEastLimit) */
    private static String cityDelimitation;

    private static String parksQuery;
    private static String roadsQuery;
    private static String waterQuery;
    private static String buildingsQuery;
    private static String landuseQuery;
    private static String railwaysQuery;
    private static String transportLinesQuery;


    /**
     * Specify the bounding coordinates of the city that the user has chosen.
     * @param   city        City that the user has chosen.
     * @since   1.3
     */
    private static void getCityCoords(City city) {

        // Set the limits in coordinates for our query
        citySouthLimit = city.lat - SQUARE_RADIUS;
        cityNorthLimit = city.lat + SQUARE_RADIUS;
        cityWestLimit = city.lon - SQUARE_RADIUS;
        cityEastLimit = city.lon + SQUARE_RADIUS;

        // Used in the queries
        cityDelimitation = "(" + citySouthLimit + ", " + cityWestLimit + ", " + cityNorthLimit + ", " + cityEastLimit + ")";
    }

    /**
     * Makes the directory in which the XML files will be stored.
     * @param   city        Name of the city that the user has chosen.=
     * @since   1.13.1
     */
    private static void makeCityDir(String country, String city) {
        File cityDirectory;

        // Check if the city's directory exists, if not create it
        if (country.isEmpty()) {
            cityDirectory = new File(DIRNAME + "/- No country/" + city);
        }
        else {
            cityDirectory = new File(DIRNAME + "/" + country + "/" + city);
        }

        if (!cityDirectory.exists()) {
            cityDirectory.mkdirs();
        }
    }

    private static void initRequests() {

        parksQuery      = "[out:xml];"
                        + "("
                        + " way[\"leisure\"~\"^(garden|park|golf_course|pitch|playground)$\"]" + cityDelimitation + ";"
                        + " way[\"amenity\"~\"^(grave_yard|cemetery)$\"]" + cityDelimitation + ";"
                        + " relation[\"leisure\"~\"^(garden|park|golf_course|pitch|playground)$\"]" + cityDelimitation + ";"
                        + " relation[\"amenity\"~\"^(grave_yard|cemetery)$\"]" + cityDelimitation + ";"
                        + ");"
                        + "out geom;";

        roadsQuery      = "[out:xml];"
                        + "way[\"highway\"]" + cityDelimitation + ";"
                        + "out geom;";

        waterQuery      = "[out:xml];"
                        + "("
                        + "  way[\"natural\"~\"^water|bay$\"]" + cityDelimitation + ";"
                        + "  way[\"landuse\"~\"^basin|reservoir$\"]" + cityDelimitation + ";"
                        + "  way[\"leisure\"~\"^swimming_pool$\"]" + cityDelimitation + ";"
                        + "  way[\"waterway\"~\"^riverbank|stream|canal|drain|ditch$\"]" + cityDelimitation + ";"
                        + "  relation[\"natural\"~\"^water|bay$\"]" + cityDelimitation + ";"
                        + "  relation[\"landuse\"~\"^basin|reservoir$\"]" + cityDelimitation + ";"
                        + "  relation[\"leisure\"~\"^swimming_pool$\"]" + cityDelimitation + ";"
                        + "  relation[\"waterway\"~\"^riverbank$\"]" + cityDelimitation + ";"
                        + ");"
                        + "out geom;";

        buildingsQuery      = "[out:xml];"
                            + "("
                            + "  way[\"building\"]" + cityDelimitation + ";"
                            + "  relation[\"building\"]" + cityDelimitation + ";"
                            + ");"
                            + "out geom;";

        landuseQuery    = "[out:xml];"
                        + "("
                        + " ("
                        + "     way[\"landuse\"]" + cityDelimitation + ";"
                        + "     relation[\"landuse\"]" + cityDelimitation + ";"
                        + " );"
                        + "-"
                        + " ("
                        + "     way[\"landuse\"~\"^basin|reservoir$\"]" + cityDelimitation + ";"
                        + "     relation[\"landuse\"~\"^basin|reservoir$\"]" + cityDelimitation + ";"
                        + " );"
                        + ");"
                        + "out geom;";

        railwaysQuery = "[out:xml];"
                        + "way[\"railway\"]" + cityDelimitation + ";"
                        + "out geom;";

        transportLinesQuery = "[out:xml];"
                            + "("
                            + " relation[\"route\"=\"bus\"]" + cityDelimitation + ";"
                            + " relation[\"route\"=\"tram\"]" + cityDelimitation + ";"
                            + ");"
                            + "out geom;";
    }


    /**
     * Delete all xml files inside the data storage directory.
     * @since   1.6
     */
    private static void deleteXMLs(String dirPath) {
        Interface.appendToStateLn("Deleting old data... ");

        long startTime = System.nanoTime();

        File directory = new File(dirPath);
        for (File file: Objects.requireNonNull(directory.listFiles())) {
            if (file.getName().endsWith(".xml"))
                file.delete();
        }

        long endTime = System.nanoTime();
        long elapsedTime = (endTime - startTime) / 1000000000;

        Interface.appendToStateLn("\tDone in " + elapsedTime + "s.");
    }


    /**
     * Calls all the methods that generate XML files.
     * @param   city        Name of the city that the user has chosen.
     * @since   1.3
     */
    static void generateAllXMLs(City city) throws InterruptedException {
        Interface.appendToStateLn("Generating '" + city.name + "' data from OpenStreetMap database... ");

        makeCityDir(city.country, city.name);
        getCityCoords(city);
        initRequests();

        // Create the thread pool (list of threads)
        List<Thread> threadPool = new ArrayList<>();

        // Create the threads

        // Send the water request if the water.xml file does not exist, and until a successful response is received
        threadPool.add(new Thread(new Requester(city, waterQuery, "water", "xml")));

        // Send the roads request if the roads.xml file does not exist, and until a successful response is received
        threadPool.add(new Thread(new Requester(city, roadsQuery, "roads", "xml")));

        // Send the buildings request if the buildings.xml file does not exist, and until a successful response is received
        threadPool.add(new Thread(new Requester(city, buildingsQuery, "buildings", "xml")));

        // Send the parks request if the parks.xml file does not exist, and until a successful response is received
        threadPool.add(new Thread(new Requester(city, parksQuery, "parks", "xml")));

        // Send the transport lines request if the transport_lines.xml file does not exist, and until a successful response is received
        threadPool.add(new Thread(new Requester(city, transportLinesQuery, "transport_lines", "xml")));

        // If the map beautification has been enabled, then query some more data :
        if (Interface.enableMapBeautification) {
            // Send the landuse request if the landuse.xml file does not exist, and until a successful response is received
            threadPool.add(new Thread(new Requester(city, landuseQuery, "landuse", "xml")));

            // Send the railways request if the railways.xml file does not exist, and until a successful response is received
            threadPool.add(new Thread(new Requester(city, railwaysQuery, "railways", "xml")));
        }

        // Start the threads
        for (Thread thread : threadPool) {
            thread.start();
        }

        // Wait for each thread to finish before continuing
        for (Thread thread : threadPool) {
            thread.join();
        }

        Interface.appendToStateLn("\t→ Done generating the data.");
    }

}
