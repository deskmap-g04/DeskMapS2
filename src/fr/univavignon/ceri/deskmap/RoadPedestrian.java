package fr.univavignon.ceri.deskmap;

import java.util.List;

class RoadPedestrian extends Road {

    RoadPedestrian(long id, List<Node> nodes, String name, String type, double layer, int priorityInLayer, int speedLimit, boolean isOneWay, double[] bounds) {
        super(id, nodes, name, type, layer, priorityInLayer, speedLimit, isOneWay, true, bounds);

        initDrawingParameters(MapColor.pedestrianOutline, MapColor.pedestrianOutlineDisabled, DrawableMetadata.smallSize, DrawableMetadata.largeZoomThreshold);
    }
}
