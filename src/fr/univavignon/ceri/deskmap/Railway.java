package fr.univavignon.ceri.deskmap;

import java.util.List;

class Railway extends DrawableDashedLine {
    Railway(long id, List<Node> nodes, String name, String type) {
        super(id, nodes, name, type, 20);

        initDrawingParameters(MapColor.lightGray, MapColor.gray, DrawableMetadata.smallSize, DrawableMetadata.maxZoomThreshold);
    }
}
