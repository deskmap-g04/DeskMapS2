package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;

import java.util.ArrayList;
import java.util.List;

abstract class Road extends Drawable {

    static ArrayList<String> roadNames = new ArrayList<>();

    double minLat;
    double minLon;
    double maxLat;
    double maxLon;

    double distance;
    int speedLimit;

    boolean isOneWay;

    boolean pedestriansAllowed = true;
    boolean cyclistsAllowed = true;
    boolean motoristsAllowed;

    Color currentColor;
    Color disabledOutlineColor;


    Road(long id, List<Node> nodes, String name, String type, double layer, int priorityInLayer, int speedLimit, boolean isOneWay, boolean motoristsAllowed, double[] bounds) {
        super(id, nodes, name, type, layer, priorityInLayer);
        this.speedLimit = speedLimit;
        this.isOneWay = isOneWay;
        this.motoristsAllowed = motoristsAllowed;

        minLat = bounds[0];
        minLon = bounds[1];
        maxLat = bounds[2];
        maxLon = bounds[3];

        if (speedLimit > 50) {
            pedestriansAllowed = false;
            cyclistsAllowed = false;
        }

        if (!name.equals("")) {
            roadNames.add(name);
        }
    }

    Road(Road road) {
        this(road.id, road.nodes, road.name, road.type, road.layer, road.priorityInLayer, road.speedLimit, road.isOneWay, road.motoristsAllowed, new double[] {road.minLat, road.minLon, road.maxLat, road.maxLon});
    }

    void initDrawingParameters(Color outlineColor, Color disabledOutlineColor, Size size, int zoomLevelThreshold) {
        super.initDrawingParameters(outlineColor, size, zoomLevelThreshold);
        this.disabledOutlineColor = disabledOutlineColor;
        currentColor = outlineColor;
    }

    boolean overlaps(Road road) {
        return ((this.minLon <= road.maxLon) && (road.minLon <= this.maxLon) && (this.minLat <= road.maxLat) && (road.minLat <= this.maxLat));
    }

    void draw() {
        if (MapHandler.zoomLevel >= zoomLevelThreshold) {
            if (nodes.size() > 1 && checkIfIsInBbox()) {
                Interface.gc.setLineWidth(size.get());
                Interface.gc.setStroke(currentColor);

                Node firstNode = nodes.get(0);
                Interface.gc.moveTo(firstNode.xPos, firstNode.yPos);

                Interface.gc.beginPath();
                for (Node node : nodes) {
                    Interface.gc.lineTo(node.xPos, node.yPos);
                }

                Interface.gc.stroke();
            }
        }
    }

    void updateRoadColors(String transportMode) {
        switch (transportMode) {
            case "Car":
                if (motoristsAllowed) {
                    currentColor = outlineColor;
                }
                else {
                    currentColor = disabledOutlineColor;
                }
                break;

            case "Bike":
                if (cyclistsAllowed) {
                    currentColor = outlineColor;
                }
                else {
                    currentColor = disabledOutlineColor;
                }
                break;

            case "Pedestrian":
                if (pedestriansAllowed) {
                    currentColor = outlineColor;
                }
                else {
                    currentColor = disabledOutlineColor;
                }
                break;

            default:
                currentColor = outlineColor;
        }
    }
}
