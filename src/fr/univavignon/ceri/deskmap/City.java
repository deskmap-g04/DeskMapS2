package fr.univavignon.ceri.deskmap;

class City extends Node {

    String name;

    String country;
    String state;

    City(long id, String name, String country, String state, double lon, double lat) {
        super(id, lon, lat);

        this.name = name;

        this.country = country;
        this.state = state;
    }

    @Override
    public String toString() {
        String toReturn = name;
        boolean hasCountry = !country.isEmpty();
        boolean hasState = !state.isEmpty();

        if (hasCountry && hasState)
            toReturn += " (" + state + ", " + country + ")";
        else if (hasCountry)
            toReturn += " (" + country + ")";
        else if (hasState)
            toReturn += " (" + state + ")";

        return toReturn;
    }
}
