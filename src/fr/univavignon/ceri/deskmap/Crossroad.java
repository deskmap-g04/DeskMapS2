package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

class Crossroad extends DrawablePoint {

    HashMap<Crossroad, Road> neighbours = new HashMap<>();
    List<String> streetNames = new ArrayList<>();

    // A*-algorithm related
    private double g; // G cost (distance from starting node in the A* algorithm)
    private double h; // H cost (distance from end node in the A* algorithm)
    double f; // F cost = G cost + H cost ('full cost')

    Crossroad parent;
	
    Crossroad(Node node, String name) {
        super(node);
        addRoadName(name);

        resetDrawingParameters();
    }

    void addRoadName(String name) {
        if (name.isEmpty()) {
            name = "<Missing road name(s)>";
        }
        if (!streetNames.contains(name)) {
            streetNames.add(name);
        }
    }

    void applySelectedDrawingParameters(boolean isDeparture) {
        if (isDeparture) {
            initDrawingParameters(MapColor.departureColor, MapColor.departureColor, DrawableMetadata.maxZoomThreshold, 25);
        }
        else {
            initDrawingParameters(MapColor.arrivalColor, MapColor.arrivalColor, DrawableMetadata.maxZoomThreshold, 25);
        }
    }

    void resetDrawingParameters() {
        initDrawingParameters(MapColor.crossroadColor, MapColor.crossroadColor, DrawableMetadata.XSZoomThreshold, 15);
    }

    // A*-algorithm related
    void calculateHeuristic(Crossroad finalNode, String calculationMode, String transportMode) {
        double distance = DistanceCalculator.getDistanceInMeters(this.node, finalNode.node);

        if (calculationMode.equals("Distance")) {
            h = distance;
        }
        else {  // calculationMode == Time
            h = distance / SpeedLimit.getMaxSpeedInMeters(transportMode);
        }
    }

    void setNodeData(Crossroad currentNode, double cost) {
        parent = currentNode;
        g = currentNode.g + cost;
        calculateFinalCost();
    }

    boolean checkBetterPath(Crossroad currentNode, double cost) {
        double gCost = currentNode.g + cost;
        if (gCost < g) {
            setNodeData(currentNode, cost);
            return true;
        }
        return false;
    }

    private void calculateFinalCost() {
        f = g + h;
    }

    @Override
    public boolean equals(Object obj) {
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Crossroad other = (Crossroad) obj;
        return (this.node.lat == other.node.lat && this.node.lon == other.node.lon);
    }

    @Override
    public String toString() {
        return "Node at: (" + node.lat + " " + node.lon + ")";
    }
}

