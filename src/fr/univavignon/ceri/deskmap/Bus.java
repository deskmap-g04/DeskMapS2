package fr.univavignon.ceri.deskmap;

class Bus extends DrawableIcon {

    String destination;

    Bus(String name, String destination, Node coords) {
        super(name, coords);

        this.destination = destination;

        initDrawingParameters(Icon.bus, Icon.mediumSize, DrawableMetadata.largeZoomThreshold);
    }

    Bus(String name, Node coords){
        super(name, coords);

        initDrawingParameters(Icon.bus, Icon.mediumSize, DrawableMetadata.largeZoomThreshold);
    }
}
