package fr.univavignon.ceri.deskmap;

class Size {

    private int size;

    Size(int size) {
        this.size = size;
    }

    void set(int lineWidth) {
        this.size = lineWidth;
    }

    int get() {
        return size;
    }

    @Override
    public String toString() {
        return String.valueOf(size);
    }
}
