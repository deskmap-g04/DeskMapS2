package fr.univavignon.ceri.deskmap;

import java.io.*;
import java.net.URL;
import java.net.URLConnection;
import java.net.URLEncoder;
import java.util.Scanner;

class Requester implements Runnable {

    /** Charset used in the query */
    private static final String CHARSET = "UTF-8";

    /** Directory in which xml files will be stored */
    private static final String DIRNAME = OSMData.DIRNAME;

    private City city;
    private String overpassQuery;
    private String fileName;
    private String extension;


    Requester(City city, String overpassQuery, String fileName, String extension) {
        this.city = city;
        this.overpassQuery = overpassQuery;
        this.fileName = fileName;
        this.extension = extension;
    }

    public void run() {
        generateFileFromOverpassResponse(city, overpassQuery, fileName, extension);
    }

    /**
     * General method used to generate a file containing the result of an Overpass query.
     * @param   city            Name of the city.
     * @param   overpassQuery   Query to send to Overpass.
     * @param   fileName        Name of the file to create (without the ".<extension>").
     * @param   extension       Extension of the file (ex: "xml", "csv", ...).
     * @since   1.12
     */
    private static void generateFileFromOverpassResponse(City city, String overpassQuery, String fileName, String extension) {

        // Check if the file exists, and create it only if NOT
        String fullFilePathNameAndExtension;
        if (city.country.isEmpty())
            fullFilePathNameAndExtension = DIRNAME + "/- No country/" + city.name + "/" + fileName + "." + extension;
        else
            fullFilePathNameAndExtension = DIRNAME + "/" + city.country + "/" + city.name + "/" + fileName + "." + extension;

        File file = new File(fullFilePathNameAndExtension);

        if (!file.exists()) {
            long startTime = System.nanoTime();

            do {
                try {
                    String responseBody = sendRequest(OSMData.OVERPASS_URL, overpassQuery, true);
                    createFileFromString(fullFilePathNameAndExtension, responseBody);
                } catch (IOException e) {
//                    e.printStackTrace();
//                    Interface.appendToStateLn("\t\tServer error, trying again...");
                    System.out.println("Server error.");
                }
            } while (!new File(fullFilePathNameAndExtension).exists());

            long endTime = System.nanoTime();
            long elapsedTime = (endTime - startTime) / 1000000000;

            Interface.appendToStateLn("\tGenerated " + fileName + "." + extension + " in " + elapsedTime + "s. (" + fileName + ")");
        }
        else {
            Interface.appendToStateLn("\tFile " + fileName + "." + extension + " already exists, skipping... ");
        }
    }

    /**
     * Create a file from a String var.
     * <p>
     *     Create a file containing what the String var contains.
     *     The String should be the result of a request.
     * </p>
     * @param   fullFilePathNameAndExtension   Name of the created file (with path, file name and extension).
     * @param   content                     Result of a query.
     * @throws  IOException             If an I/O exception of some sort has occurred.
     * @since   1.1
     */
    private static void createFileFromString(String fullFilePathNameAndExtension, String content) throws IOException {
        PrintWriter outFile = new PrintWriter(new FileWriter(fullFilePathNameAndExtension));
        outFile.print(content);
        outFile.close();
    }

    /**
     * Send a request to an API (url) and returns the String response.
     * <p>
     *     Send a request to the Overpass API interpreter (http://www.overpass-api.de/api/interpreter).
     *     Converts the response to a String and returns it.
     * </p>
     * @return  the response as a String.
     * @throws  IOException             If an I/O exception of some sort has occurred.
     * @since   1.2
     */
    static String sendRequest(String url, String query, boolean encodeRequest) throws IOException {
        // Encode the query if needed
        if (encodeRequest)
            query = encode(query);

        // Send the request
        URLConnection connection = new URL(url + query).openConnection();
        connection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:25.0) Gecko/20100101");
        connection.setRequestProperty("Accept-Charset", CHARSET);

        // Get the response
        InputStream response = connection.getInputStream();

        // Scan the response
        Scanner scanner = new Scanner(response);

        // "\A" stands for "start of a string"
        return scanner.useDelimiter("\\A").next();
    }

    static String encode(String toEncode) throws UnsupportedEncodingException {
        return URLEncoder.encode(toEncode, "UTF8");
    }
}
