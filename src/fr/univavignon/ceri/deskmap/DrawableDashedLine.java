package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;
import javafx.scene.shape.StrokeLineCap;

import java.util.List;

abstract class DrawableDashedLine extends Drawable {

    private double dashSpace;

    private Color lineBackgroundColor;
    private Color dashColor;

    DrawableDashedLine(long id, List<Node> nodes, String name, String type, double dashSpace) {
        super(id, nodes, name, type);

        this.dashSpace = dashSpace;
    }

    void initDrawingParameters(Color lineBackgroundColor, Color dashColor, Size size, int zoomLevelThreshold) {
        super.initDrawingParameters(dashColor, size, zoomLevelThreshold);

        this.lineBackgroundColor = lineBackgroundColor;
        this.dashColor = dashColor;
    }

    @Override
    void draw() {

        // Draw the background
        outlineColor = lineBackgroundColor;
        super.draw();

        // Draw the dashes
        outlineColor = dashColor;
        Interface.gc.setLineDashes(dashSpace);
        Interface.gc.setLineCap(StrokeLineCap.BUTT);

        super.draw();

        Interface.gc.setLineDashes(0);
        Interface.gc.setLineCap(StrokeLineCap.ROUND);
    }
}
