package fr.univavignon.ceri.deskmap;

class DrawableMetadata {
    static Size basicSize = new Size(1);
    static Size itinerarySize = new Size(1);
    static Size XSSize = new Size(1);
    static Size smallSize = new Size(1);
    static Size mediumSize = new Size(2);
    static Size largeSize = new Size(3);

    static int X5SZoomThreshold = 18;
    static int X4SZoomThreshold = 15;
    static int X3SZoomThreshold = 12;
    static int XXSZoomThreshold = 9;
    static int XSZoomThreshold = 6;
    static int smallZoomThreshold = 4;
    static int mediumZoomThreshold = 2;
    static int largeZoomThreshold = 0;
    static int XLZoomThreshold = -2;
    static int XXLZoomThreshold = -4;
    static int maxZoomThreshold = -6;

    static int motorwayPriority = 100;
    static int trunkPriority = 90;
    static int primaryPriority = 80;
    static int secondaryPriority = 70;
    static int tertiaryPriority = 60;
    static int residentialPriority = 50;
    static int livingStreetPriority = 40;
    static int pedestrianPriority = 30;
    static int servicePriority = 20;
    static int trackPriority = 10;
    static int cyclewayPriority = 7;
    static int bridlewayPriority = 5;
    static int footwayPriority = 3;

    static void adaptToCurrentZoomLevel(int zoomLevel) {
        if (zoomLevel >= X5SZoomThreshold) {
            itinerarySize.set(25);
            XSSize.set(15);
            smallSize.set(25);
            mediumSize.set(35);
            largeSize.set(45);
        }
        else if (zoomLevel >= X4SZoomThreshold) {
            itinerarySize.set(18);
            XSSize.set(10);
            smallSize.set(18);
            mediumSize.set(28);
            largeSize.set(37);
        }
        else if (zoomLevel >= X3SZoomThreshold) {
            itinerarySize.set(11);
            XSSize.set(7);
            smallSize.set(11);
            mediumSize.set(22);
            largeSize.set(30);
        }
        else if (zoomLevel >= XXSZoomThreshold) {
            itinerarySize.set(8);
            XSSize.set(4);
            smallSize.set(6);
            mediumSize.set(15);
            largeSize.set(22);
        }
        else if (zoomLevel >= XSZoomThreshold) {
            itinerarySize.set(6);
            XSSize.set(2);
            smallSize.set(3);
            mediumSize.set(8);
            largeSize.set(14);
        }
        else if (zoomLevel >= smallZoomThreshold) {
            itinerarySize.set(4);
            XSSize.set(1);
            smallSize.set(2);
            mediumSize.set(5);
            largeSize.set(8);
        }
        else if (zoomLevel >= mediumZoomThreshold) {
            itinerarySize.set(3);
            XSSize.set(1);
            smallSize.set(1);
            mediumSize.set(3);
            largeSize.set(4);
        }
        else {
            itinerarySize.set(2);
            XSSize.set(1);
            smallSize.set(1);
            mediumSize.set(2);
            largeSize.set(3);
        }
    }
}
