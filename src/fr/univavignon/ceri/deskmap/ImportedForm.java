package fr.univavignon.ceri.deskmap;

import java.util.List;

public class ImportedForm {
	
	private City city = null;
	private String departureNum = "";
	private String departureAddress = "";
	private String arrivalNum = "";
	private String arrivalAddress = "";
	private List<String> informations = null;
	
	ImportedForm(City city, String departureNum, String departureAddress, String arrivalNum, String arrivalAddress, List<String> informations)
	{
		this.city = city;
		this.departureNum = departureNum;
		this.departureAddress = departureAddress;
		this.arrivalNum = arrivalNum;
		this.arrivalAddress = arrivalAddress;
		this.informations = informations;
	}
	
	public String getCity()
	{
		return this.getCity();
	}
	
	public String toString() 
	{
		city.toString();		
		System.out.println("Departure : "+departureNum+departureAddress+" -> Arrival : "+arrivalNum+" "+arrivalAddress);
		
	    for(String s : informations)
        {
	    	System.out.println(s);
        }
		return null;
	}
	
	public void updateAfterImport()
	{
		Interface.disableFields(false);
		Interface.cities.setText(this.city.toString());
		Interface.numDep.setText(this.departureNum);
		Interface.numArr.setText(this.arrivalNum);
		Interface.roadNameDep.setText(this.departureAddress);
		Interface.roadNameArr.setText(this.arrivalAddress);
	}
}
