package fr.univavignon.ceri.deskmap;

import javax.swing.*;
import java.io.File;

public class FileManager {

    static File openItinerary() {
        // Make the save directory if not exists
        File itineraryDirectory = new File("Itineraries");
        itineraryDirectory.mkdirs();

        // Create the parent component of the dialog
        JFrame parentFrame = new JFrame();

        // Create the file chooser
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Open a file");
        fileChooser.setCurrentDirectory(itineraryDirectory);

        int userSelection = fileChooser.showOpenDialog(parentFrame);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToOpen = fileChooser.getSelectedFile();
            System.out.println("Selected: " + fileToOpen.getAbsolutePath());
            return fileToOpen;
        }
        return null;
    }

    static File saveItinerary(City city) {
        // Make the save directory if not exists
        File itineraryDirectory = new File("Itineraries");
        itineraryDirectory.mkdirs();

        // Create the parent component of the dialog
        JFrame parentFrame = new JFrame();

        // Create the file chooser
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setDialogTitle("Save a file");
        fileChooser.setCurrentDirectory(itineraryDirectory);
        fileChooser.setSelectedFile(new File("itinerary.xml"));

        int userSelection = fileChooser.showSaveDialog(parentFrame);

        if (userSelection == JFileChooser.APPROVE_OPTION) {
            File fileToSave = fileChooser.getSelectedFile();
            System.out.println("Selected: " + fileToSave.getAbsolutePath());
            XmlManager.export(city,fileToSave);
            return fileToSave;
        }

        return null;
    }
}
