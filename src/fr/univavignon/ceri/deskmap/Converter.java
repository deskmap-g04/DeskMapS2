package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.List;

class Converter {
    private static double lonMin;
    private static double lonMax;
    private static double latMin;
    private static double latMax;


    private static double xMinBeforeZoom;
    private static double xMaxBeforeZoom;
    private static double yMinBeforeZoom;
    private static double yMaxBeforeZoom;

    private static double widthBeforeZoom;

    private static double width;
    private static double height;

    static List<List<Drawable>> map;
    static List<List<DrawableIcon>> refreshableMapPoints;
    static List<List<DrawableIcon>> otherMapPoints;
    static List<Crossroad> mapCrossroads;

    static Itinerary itinerary;


    static void init(DataContainer DT) {
        Interface.appendToState("Initializing the converter...");
        map = new ArrayList<>();

        if (Interface.enableMapBeautification) {
            map.add(DT.landuse);
        }
        map.add(DT.parks);
        map.add(DT.water);
        map.add(DT.buildings);
        map.add(DT.roads);
        map.add(DT.transportLines);
        if (Interface.enableMapBeautification) {
            map.add(DT.railways);
        }

        refreshableMapPoints = new ArrayList<>();
        if (DT.city.name.equals("Angers") && DT.city.country.equals("France")) {
            refreshableMapPoints.add(DT.busStops);
            refreshableMapPoints.add(DT.buses);
        }

        otherMapPoints = new ArrayList<>();
        otherMapPoints.add(DT.oneWayArrows);

        mapCrossroads = DT.activeCrossroads;

        itinerary = DT.itinerary;

        lonMin = OSMData.cityWestLimit;
        lonMax = OSMData.cityEastLimit;
        latMin = OSMData.citySouthLimit;
        latMax = OSMData.cityNorthLimit;

        initXY(Interface.canvasMap.getWidth(), Interface.canvasMap.getHeight());
        initXYBeforeZoom();

        double scaleToCanvasRatio = MapScale.scaleWidth / Interface.canvasMap.getWidth();
        double canvasWidthInMeters = DistanceCalculator.getDistanceInMeters(latMin, lonMin, latMin, lonMax);
        MapScale.scaleInMeters = scaleToCanvasRatio * canvasWidthInMeters;

        Interface.appendToStateLn(" Done.");
    }

    private static void initXY(double width, double height) {
        Converter.width = width;
        Converter.height = height;
    }
    
    private static void refreshBuses(DataContainer DT) {
        refreshableMapPoints = new ArrayList<>();
        refreshableMapPoints.add(DT.buses);
    }

    private static void initXYBeforeZoom() {
        xMinBeforeZoom = calculateXPosMercator(lonMin);
        xMaxBeforeZoom = calculateXPosMercator(lonMax);
        yMinBeforeZoom = calculateYPosMercator(latMin);
        yMaxBeforeZoom = calculateYPosMercator(latMax);

        widthBeforeZoom = xMaxBeforeZoom - xMinBeforeZoom;
    }

    static void recalculateAllXYPos() {
        Interface.appendToStateLn("Converting lon/lat to x/y coords... ");

        long startTime = System.nanoTime();

        // Map (parks, water, buildings, roads, landuse)
        for (List<Drawable> list : map) {
            for (Drawable drawable : list) {
                for (Node node : drawable.nodes) {
                    recalculateInitialXYPos(node);
                }
            }
        }

        // Map points (buses, intersections)
        for (List<DrawableIcon> list : refreshableMapPoints) {
            if (list != null) {
                for (DrawableIcon drawableIcon : list) {
                    recalculateInitialXYPos(drawableIcon.node);
                }
            }
        }

        long endTime = System.nanoTime();
        long elapsedTime = (endTime - startTime) / 1000000000;

        Interface.appendToStateLn("\tDone in " + elapsedTime + "s.");
    }

    static void recalculateDrawablePointsXYPos() {
        // Prevent the user to move on the map while the coords are being recalculated
        MapHandler.isReady = false;

        // Recalculate the position of the objects according to the current position and zoom level on the map
        for (List<DrawableIcon> list : refreshableMapPoints) {
            for (DrawableIcon drawableIcon : list) {
                // Initialize the position of the node as if the user hadn't moved it
                recalculateInitialXYPos(drawableIcon.node);

                // Adapt it's position to the current x/y offset and the current zoom level
                MapHandler.adaptPositionToCurrent(drawableIcon.node);
            }
        }
    }
    

    static void recalculateDrawablePointsXYPos(DataContainer DT) {
        // Refresh the map points
        refreshBuses(DT);

        // Prevent the user to move on the map while the coords are being recalculated
        MapHandler.isReady = false;

        // Recalculate the position of the objects according to the current position and zoom level on the map
        for (List<DrawableIcon> list : refreshableMapPoints) {
            for (DrawableIcon drawableIcon : list) {
                // Initialize the position of the node as if the user hadn't moved it
                recalculateInitialXYPos(drawableIcon.node);

                // Adapt it's position to the current x/y offset and the current zoom level
                MapHandler.adaptPositionToCurrent(drawableIcon.node);
            }
        }

        // Then, re-add the non-moving drawable points (like bus stops)
        refreshableMapPoints.add(DT.busStops);
    }


    private static void recalculateInitialXYPos(Node node) {
        // Convert coords to Mercator ones
        double tempX = calculateXPosMercator(node.lon);
        double tempY = calculateYPosMercator(node.lat);

        // Recenter the x/y coords
        tempX -= xMinBeforeZoom;
        tempY -= yMinBeforeZoom;

        // Zoom in
        tempX *= (width / widthBeforeZoom);
        tempY *= -(width / widthBeforeZoom); // Not height, intentional. It's because of the deformations in lat when getting closer to the poles

        // Set node x and y pos and revert the y axis as the canvas' origin point is on the upper left corner
        node.xPos = tempX;
        node.yPos = height - tempY;
    }

    private static double calculateXPosMercator(double xPos) {
        return (xPos + 180) * (width / 360);
    }

    private static double calculateYPosMercator(double yPos) {
        double latRad = yPos * Math.PI / 180;
        double mercN = Math.log(Math.tan((Math.PI / 4) + (latRad / 2)));
        return height - (height / 2) - (width * mercN / (2 * Math.PI));
    }

    public static int convertLatitudeToMeter(double heightLineScale, double canvasHeight, double minLatitude, double maxLatitude) {
        int valueMeter = (int) ((((maxLatitude - minLatitude) * 111110) * heightLineScale) / canvasHeight);
        return valueMeter;
    }

    public static int convertLongitudeToMeter(double widthLineScale, double canvasWidth, double minLongitude, double maxLongitude, double minLatitude, double maxLatitude) {
        double OneDegreLong = (111110 * Math.cos((maxLatitude - minLatitude) * Math.PI / 180));
        int valueMeter = (int) ((int) (OneDegreLong * (maxLongitude - minLongitude) * widthLineScale) / canvasWidth);
        return valueMeter;
    }

    static void calculateRoadDistances(DataContainer DT) {
        for (Road road : (List<Road>)(List<?>) DT.roads) {
            double distance = 0;
            Node previousNode = null;

            for (Node currentNode : road.nodes) {
                if (previousNode != null) {
                    distance += DistanceCalculator.getDistanceInMeters(previousNode, currentNode);
                }
                previousNode = currentNode;
            }

            road.distance = distance;
        }
    }
}

