package fr.univavignon.ceri.deskmap;

import java.lang.*;

class DistanceCalculator
{
    static double getDistanceInMeters(Node A, Node B) {
        if ((A.lat == B.lat) && (A.lon == B.lon)) {
            return 0;
        }
        else {
            double theta = A.lon - B.lon;
            double dist = Math.sin(Math.toRadians(A.lat)) * Math.sin(Math.toRadians(B.lat)) + Math.cos(Math.toRadians(A.lat)) * Math.cos(Math.toRadians(B.lat)) * Math.cos(Math.toRadians(theta));
            dist = Math.acos(dist);
            dist = Math.toDegrees(dist);
            dist = dist * 60 * 1.1515;
            dist = dist * 1609.344;
            return (dist);
        }
    }

    static double getDistanceInMeters(double aLat, double aLon, double bLat, double bLon) {
        Node A = new Node(aLon, aLat);
        Node B = new Node(bLon, bLat);

        return getDistanceInMeters(A, B);
    }
}