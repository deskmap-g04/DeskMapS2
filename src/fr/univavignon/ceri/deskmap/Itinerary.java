package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

class Itinerary extends Drawable {

    Crossroad departure;
    Crossroad arrival;

    List<Road> path = new ArrayList<>();


    Itinerary() {
        super(0, new ArrayList<>(), "Itinerary", null);

        initDrawingParameters(MapColor.itineraryColor, DrawableMetadata.itinerarySize, DrawableMetadata.maxZoomThreshold);
    }

    void makeNewItineraryFromPath(List<Road> path) {
        this.path = path;
        this.nodes = new ArrayList<>();

        for (Road road : path) {
            if (road.nodes.size() > 1) {
                List<Node> nodesToAdd = new ArrayList<>(road.nodes);

                // Reverse the order of the road if needed
                Node lastNodeFromPath;
                if (nodes.size() > 0) {
                    lastNodeFromPath = nodes.get(nodes.size() - 1);
                }
                else {
                    lastNodeFromPath = departure.node;
                }
                Node firstNodeFromNewRoad = nodesToAdd.get(0);

                if (lastNodeFromPath.lat != firstNodeFromNewRoad.lat && lastNodeFromPath.lon != firstNodeFromNewRoad.lon) {
                    Collections.reverse(nodesToAdd);
                }

                boolean isDeparture = (lastNodeFromPath == departure.node);
                if (isDeparture) {
                    nodes.addAll(nodesToAdd);
                }
                else {
                    // Add all the nodes to the itinerary (except the first, which has already been added)
                    boolean isFirstNode = true;
                    for (Node node : nodesToAdd) {
                        if (!isFirstNode) {
                            nodes.add(node);
                        }
                        else {
                            isFirstNode = false;
                        }
                    }
                }
            }
        }
    }
}
