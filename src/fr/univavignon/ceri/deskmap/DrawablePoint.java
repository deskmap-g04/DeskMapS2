package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;

class DrawablePoint {

    long id;
    private static long staticID = 0;

    Node node;

    private Color outlineColor;
    private Color fillerColor;
    int size;

    int zoomLevelThreshold;

    DrawablePoint(Node node) {
        this.node = node;

        this.id = staticID;
        staticID++;
    }

    void initDrawingParameters(Color outlineColor, Color fillerColor, int zoomLevelThreshold, int size) {
        this.outlineColor = outlineColor;
        this.fillerColor = fillerColor;
        this.zoomLevelThreshold = zoomLevelThreshold;
        this.size = size;
    }

    void draw() {
        if (MapHandler.zoomLevel >= zoomLevelThreshold) {
            if (node.xPos > 0 && node.xPos < Interface.canvasMap.getWidth()) {      // If the x coord is visible
                if (node.yPos > 0 && node.yPos < Interface.canvasMap.getHeight()) { // And if the y coord is visible
                    // Then the entire object should be drawn

                    Interface.gc.setStroke(outlineColor);
                    Interface.gc.setFill(fillerColor);

                    int xTopLeft = (int) (node.xPos - (size / 2));
                    int yTopLeft = (int) (node.yPos - (size / 2));

                    Interface.gc.fillOval(xTopLeft, yTopLeft, size, size);
                    Interface.gc.strokeOval(xTopLeft, yTopLeft, size, size);
                }
            }
        }
    }
}
