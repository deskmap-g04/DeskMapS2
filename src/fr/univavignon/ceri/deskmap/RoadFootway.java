package fr.univavignon.ceri.deskmap;

import java.util.List;

class RoadFootway extends DashedRoad {

    RoadFootway(long id, List<Node> nodes, String name, String type, double layer, int priorityInLayer, int speedLimit, boolean isOneWay, double[] bounds) {
        super(id, nodes, name, type, layer, priorityInLayer, speedLimit, isOneWay, false, bounds);

        this.dashSpace = 3;

        initDrawingParameters(MapColor.footwayOutline, MapColor.footwayOutlineDisabled, DrawableMetadata.basicSize, DrawableMetadata.XSZoomThreshold);
    }
}
