package fr.univavignon.ceri.deskmap;

import javafx.scene.image.Image;

class DrawableIcon {

    long id;
    private static long staticID = 0;

    Node node;

    String name;

    Image icon;
    int iconSize;
    int zoomLevelThreshold;

    DrawableIcon(String name, Node node) {
        this.name = name;
        this.node = node;

        this.id = staticID;
        staticID++;
    }

    DrawableIcon(String name, Node node, long id) {
        this.name = name;
        this.node = node;

        this.id = id;
    }

    void initDrawingParameters(Image icon, int iconSize, int zoomLevelThreshold) {
        this.icon = icon;
        this.iconSize = iconSize;
        this.zoomLevelThreshold = zoomLevelThreshold;
    }

    void draw() {
        if (MapHandler.zoomLevel >= zoomLevelThreshold) {
            if (node.xPos > 0 && node.xPos < Interface.canvasMap.getWidth()) {      // If the x coord is visible
                if (node.yPos > 0 && node.yPos < Interface.canvasMap.getHeight()) { // And if the y coord is visible
                    int xTopLeft = (int) (node.xPos - (iconSize / 2));
                    int yTopLeft = (int) (node.yPos - (iconSize / 2));

                    Interface.gc.drawImage(icon, xTopLeft, yTopLeft, iconSize, iconSize);
                }
            }
        }
    }
}
