package fr.univavignon.ceri.deskmap;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.json.*;

class CityDetailsAutoCompleter {

    /** Photon interpreter URL */
    private static final String PHOTON_URL = "http://photon.komoot.de/api/?q=";

    static List<City> suggestions;


    static void generateCitySuggestionsFrom(String cityName) throws IOException {
        suggestions = new ArrayList<>();

        String query = Requester.encode("\"" + cityName + "\"") + "&limit=100";
        String requestResult = Requester.sendRequest(PHOTON_URL, query, false);

        JSONObject object = new JSONObject(requestResult);
        JSONArray array = object.getJSONArray("features");
        for (int i = 0; i < array.length(); i++) {

            JSONObject currentObj = array.getJSONObject(i);
            JSONObject properties = currentObj.getJSONObject("properties");

            // Verify if the current object is a place
            if (properties.getString("osm_key").equals("place")) {

                // Verify if the current object is either a city, a town or a village
                String osmValue = properties.getString("osm_value");
                if (osmValue.equals("city") || osmValue.equals("town") || osmValue.equals("village")) {

                    long id = properties.getLong("osm_id");
                    String name = properties.getString("name");

                    String country = "";
                    String state = "";

                    try {
                        country = properties.getString("country");
                    } catch (JSONException ignored) {  }

                    try {
                        state = properties.getString("state");
                    } catch (JSONException ignored) {  }

                    JSONArray geometryArray = currentObj.getJSONObject("geometry").getJSONArray("coordinates");
                    double lon = geometryArray.getDouble(0);
                    double lat = geometryArray.getDouble(1);

                    suggestions.add(new City(id, name, country, state, lon, lat));
                }
            }
        }
    }
}
