package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;

import java.util.List;

public class TransportLine extends Drawable {

    boolean visible = false;
    String hexaColorCode;

    List<List<Node>> segments;

    String ref;
    String from;
    String to;


    TransportLine(long id, List<Node> nodes, List<List<Node>> segments, String name, String type, String ref, String from, String to, Color color) {
        super(id, nodes, name, type);

        this.segments = segments;
        this.ref = ref;
        this.from = from;
        this.to = to;
        BusLinesLink.checkIfExistsAndAddOSMList(ref);

        initDrawingParameters(color, DrawableMetadata.smallSize);
        hexaColorCode = "#" + outlineColor.toString().substring(2, 8).toUpperCase();
    }

    static Color getRefColor(List<TransportLine> transportLines, String ref) {
        for (TransportLine transportLine : transportLines) {
            if (transportLine.ref.equals(ref)) {
                return transportLine.outlineColor;
            }
        }
        return ColorGenerator.generateRandomTransportLineColor();
    }

    @Override
    void draw() {
        if (!visible) {
            return;
        }
        if (MapHandler.zoomLevel >= zoomLevelThreshold) {
            for (List<Node> segment : segments) {
                if (segment.size() > 1 && checkIfIsInBbox(segment)) {
                    Interface.gc.setLineWidth(size.get());
                    Interface.gc.setStroke(outlineColor);

                    Node firstNode = segment.get(0);
                    Interface.gc.moveTo(firstNode.xPos, firstNode.yPos);

                    Interface.gc.beginPath();
                    for (Node node : segment) {
                        Interface.gc.lineTo(node.xPos, node.yPos);
                    }

                    Interface.gc.stroke();
                }
            }
        }
    }

    boolean checkIfIsInBbox(List<Node> segment) {
        for (Node node : segment) {
            if (node.xPos > 0 && node.xPos < Interface.canvasMap.getWidth()) {      // If the x coord is visible
                if (node.yPos > 0 && node.yPos < Interface.canvasMap.getHeight()) { // And if the y coord is visible
                    return true;                                                    // Then the entire object should be drawn
                }
            }
        }
        return false;
    }

    //    @Override
//    void draw() {
//        // Still in development
//        if (lineIndex == 5) {
//            super.draw();
////            System.out.println("Line " + lineIndex + " (" + name + ") :");
////            for (Node node : nodes) {
////                System.out.println("\t(" + node.lat + ", " + node.lon + ") = (" + node.xPos + ", " + node.yPos + ")");
////            }
////            System.out.println();
//        }
//    }

//    @Override
//    public String toString() {
//        return id + " - " + name + " (type: " + type + ", from: " + from + ", to: " + to + ", ref: " + ref + "): " + nodes.size() + " nodes";
//    }
}
