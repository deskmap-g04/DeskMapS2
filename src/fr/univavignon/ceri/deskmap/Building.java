package fr.univavignon.ceri.deskmap;

import java.util.List;

class Building extends Fillable {

    private String name;
    private String type;


    Building(long id, List<Node> nodes, String name, String type) {
        super(id, nodes, name, type);

        initDrawingParameters(MapColor.buildingOutline, MapColor.buildingFiller, DrawableMetadata.XSZoomThreshold);
    }
}
