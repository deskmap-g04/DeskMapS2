package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

class Parser {

    private static List<String> importantTags;


    static void init() {
        importantTags = new ArrayList<>();

        importantTags.add("highway");
        importantTags.add("leisure");
        importantTags.add("amenity");
        importantTags.add("natural");
        importantTags.add("waterway");
        importantTags.add("building");
        importantTags.add("landuse");
        importantTags.add("railway");
    }

    static DataContainer parseOSMCityData(City city) throws InterruptedException {
        Interface.appendToStateLn("Parsing the data...");

        DataContainer DT = new DataContainer(city);

        // Create the thread pool (list of threads)
        List<Thread> threadPool = new ArrayList<>();

        // Create the threads

        // PARKS
        Runnable parksTask = () -> {
            try {
                double startTime = System.nanoTime();

                DT.parks = Parser.parseSpecificType(city, "parks");

                double endTime = System.nanoTime();

                int elapsedTime = (int)((endTime - startTime)/ 1000000000);

                Interface.appendToStateLn("\tParsed parks. Done in " + elapsedTime + "s");
            } catch (ParserConfigurationException | IOException | SAXException e) {
                e.printStackTrace();
            }
        };

        // WATER
        Runnable waterTask = () -> {
            try {
                double startTime = System.nanoTime();

                DT.water = Parser.parseSpecificType(city, "water");

                double endTime = System.nanoTime();

                int elapsedTime = (int)((endTime - startTime)/ 1000000000);

                Interface.appendToStateLn("\tParsed water places. Done in " + elapsedTime + "s");
            } catch (ParserConfigurationException | IOException | SAXException e) {
                e.printStackTrace();
            }
        };

        // BUILDINGS
        Runnable buildingsTask = () -> {
            try {
                double startTime = System.nanoTime();

                DT.buildings = Parser.parseSpecificType(city, "buildings");

                double endTime = System.nanoTime();

                int elapsedTime = (int)((endTime - startTime)/ 1000000000);

                Interface.appendToStateLn("\tParsed buildings. Done in " + elapsedTime + "s");
            } catch (ParserConfigurationException | IOException | SAXException e) {
                e.printStackTrace();
            }
        };

        // ROADS
        Runnable roadsTask = () -> {
            try {
                double startTime = System.nanoTime();

                DT.roads = Parser.parseSpecificType(city, "roads");
                DT.roads = Parser.subdivideRoads(DT.roads);
                DT.roads.sort(Comparator.comparingInt(o -> o.priorityInLayer));
                DT.roads.sort(Comparator.comparingInt(o -> (int) o.layer));

                double endTime = System.nanoTime();

                int elapsedTime = (int)((endTime - startTime)/ 1000000000);

                Interface.appendToStateLn("\tParsed roads. Done in " + elapsedTime + "s");
            } catch (ParserConfigurationException | IOException | SAXException | InterruptedException e) {
                e.printStackTrace();
            }
        };

        // LANDUSE
        Runnable landuseTask = () -> {
            try {
                double startTime = System.nanoTime();

                DT.landuse = Parser.parseSpecificType(city, "landuse");

                double endTime = System.nanoTime();

                int elapsedTime = (int)((endTime - startTime)/ 1000000000);

                Interface.appendToStateLn("\tParsed landuse. Done in " + elapsedTime + "s");
            } catch (ParserConfigurationException | IOException | SAXException e) {
                e.printStackTrace();
            }
        };

        // RAILWAYS
        Runnable railwaysTask = () -> {
            try {
                double startTime = System.nanoTime();

                DT.railways = Parser.parseSpecificType(city, "railways");

                double endTime = System.nanoTime();

                int elapsedTime = (int)((endTime - startTime)/ 1000000000);

                Interface.appendToStateLn("\tParsed railways. Done in " + elapsedTime + "s");
            } catch (ParserConfigurationException | IOException | SAXException e) {
                e.printStackTrace();
            }
        };

        // TRANSPORT LINES
        Runnable transportLinesTask = () -> {
            try {
                double startTime = System.nanoTime();

                DT.transportLines = Parser.parseTransportLines(city);

                double endTime = System.nanoTime();

                int elapsedTime = (int)((endTime - startTime)/ 1000000000);

                Interface.appendToStateLn("\tParsed transport lines. Done in " + elapsedTime + "s");
            } catch (ParserConfigurationException | IOException | SAXException e) {
                e.printStackTrace();
            }
        };

        threadPool.add(new Thread(parksTask));
        threadPool.add(new Thread(waterTask));
        threadPool.add(new Thread(buildingsTask));
        threadPool.add(new Thread(roadsTask));
        threadPool.add(new Thread(transportLinesTask));

        if (Interface.enableMapBeautification) {
            threadPool.add(new Thread(landuseTask));
            threadPool.add(new Thread(railwaysTask));
        }

        // Start the threads
        for (Thread thread : threadPool) {
            thread.start();
        }

        // Wait for each thread to finish before continuing
        for (Thread thread : threadPool) {
            thread.join();
        }

        Interface.appendToStateLn("\t→ Done parsing the data.");

        return DT;
    }

    private static List<Drawable> parseTransportLines(City city) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc;
        if (city.country.isEmpty())
            doc = db.parse(new FileInputStream(new File(OSMData.DIRNAME + "/- No country/" + city.name + "/transport_lines.xml")), "UTF-8");
        else
            doc = db.parse(new FileInputStream(new File(OSMData.DIRNAME + "/" + city.country + "/" + city.name + "/transport_lines.xml")), "UT F-8");

        NodeList entries = doc.getElementsByTagName("*");
        int entriesLength = entries.getLength();

        List<Drawable> transportLines = new ArrayList<>();

        long lineID = 0;
        List<Node> lineNodes = new ArrayList<>();
        List<List<Node>> lineSegments = new ArrayList<>();
        List<Node> segment = new ArrayList<>();
        String lineName = "";
        String lineRef = "";
        String lineFrom = "";
        String lineTo = "";
        String lineType = "";

        for (int i = 0; i < entriesLength; i++) {
            Element element = (Element) entries.item(i);

            // New way/relation encountered
            switch (element.getNodeName()) {
                case "relation":
                    if (lineID != 0) {
                        if (!segment.isEmpty()) {
                            lineSegments.add(segment);
                        }

                        Color lineColor = TransportLine.getRefColor((List<TransportLine>)(List<?>) transportLines, lineRef);
                        TransportLine transportLine = new TransportLine(lineID, lineNodes, lineSegments, lineName, lineType, lineRef, lineFrom, lineTo, lineColor);
                        transportLines.add(transportLine);

                        lineNodes = new ArrayList<>();
                        lineSegments = new ArrayList<>();
                        segment = new ArrayList<>();
                        lineName = "";
                        lineRef = "";
                        lineFrom = "";
                        lineTo = "";
                        lineType = "";
                    }
                    lineID = Long.parseLong(element.getAttribute("id"));
                    break;

                case "member":
                    if (!segment.isEmpty()) {
                        lineSegments.add(segment);
                    }
                    segment = new ArrayList<>();
                    break;

                case "nd":
                    double nodeLat = Double.parseDouble(element.getAttribute("lat"));
                    double nodeLon = Double.parseDouble(element.getAttribute("lon"));
                    Node node = new Node (nodeLon, nodeLat);

                    lineNodes.add(node);
                    segment.add(node);
                    break;

                case "tag":
                    String key = element.getAttribute("k");

                    switch (key) {
                        case "name":
                            lineName = element.getAttribute("v");
                            break;
                        case "ref":
                            lineRef = element.getAttribute("v");
                            break;
                        case "from":
                            lineFrom = element.getAttribute("v");
                            break;
                        case "to":
                            lineTo = element.getAttribute("v");
                            break;
                        case "route":
                            lineType = element.getAttribute("v");
                            break;
                    }
                    break;
            }
        }

        if (!segment.isEmpty()) {
            lineSegments.add(segment);
        }
        Color lineColor = TransportLine.getRefColor((List<TransportLine>)(List<?>) transportLines, lineRef);
        TransportLine transportLine = new TransportLine(lineID, lineNodes, lineSegments, lineName, lineType, lineRef, lineFrom, lineTo, lineColor);
        transportLines.add(transportLine);

        return transportLines;
    }

    private static List<Drawable> parseSpecificType(City city, String typeToParse) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);
        DocumentBuilder db = dbf.newDocumentBuilder();

        Document doc;
        if (city.country.isEmpty())
            doc = db.parse(new FileInputStream(new File(OSMData.DIRNAME + "/- No country/" + city.name + "/" + typeToParse + ".xml")), "UTF-8");
        else
            doc = db.parse(new FileInputStream(new File(OSMData.DIRNAME + "/" + city.country + "/" + city.name + "/" + typeToParse + ".xml")), "UT F-8");

        NodeList entries = doc.getElementsByTagName("*");
        int entriesLength = entries.getLength();

        List<Drawable> data = new ArrayList<>();
        List<Drawable> currentRelationMembers = new ArrayList<>();

        boolean isFirst = true;
        boolean isAMemberOfARelation = false; // True: in a relation, False: in a way
        boolean finishedParsingObject = false;
        boolean finishedParsingRelation = false;

        long currentID = 0;
        long previousID = 0;
        String currentRole = "";
        String previousRole = "";
        String currentName = "";
        String previousName = "";
        String currentType = "";
        String previousType = "";
        double currentLayer = 0;
        double previousLayer = 0;
        int currentSpeedLimit = 0;
        int previousSpeedLimit = 0;
        boolean currentIsOneWay = false;
        boolean previousIsOneWay = false;
        List<Node> currentNodes = new ArrayList<>();
        List<Node> previousNodes = new ArrayList<>();
        double[] currentBounds = new double[4];
        double[] previousBounds = new double[4];

        // Parse the XML file
        for (int i = 0; i < entriesLength; i++) {
            Element element = (Element) entries.item(i);

            // New way/relation encountered
            switch (element.getNodeName()) {
                case "relation":
                    if (!isFirst) {
                        finishedParsingObject = true;
                        previousID = currentID;
                        previousRole = currentRole;
                        previousNodes = new ArrayList<>(currentNodes);
                        currentNodes.clear();
                        isFirst = true;
                        if (isAMemberOfARelation) {
                            finishedParsingRelation = true;
                        }
                    }
                    break;

                case "member":
                    if (!isAMemberOfARelation) {
                        isAMemberOfARelation = true;
                    }
                    if (isFirst) {
                        isFirst = false;
                        currentName = "";
                        currentType = "";
                        currentLayer = 0;
                        currentSpeedLimit = 0;
                        currentIsOneWay = false;
                        currentBounds = new double[4];
                    }
                    else {
                        finishedParsingObject = true;
                        previousID = currentID;
                        previousRole = currentRole;
                        previousName = currentName;
                        currentName = "";
                        previousType = currentType;
                        currentType = "";
                        previousLayer = currentLayer;
                        currentLayer = 0;
                        previousSpeedLimit = currentSpeedLimit;
                        currentSpeedLimit = 0;
                        previousIsOneWay = currentIsOneWay;
                        currentIsOneWay = false;
                        previousNodes = new ArrayList<>(currentNodes);
                        currentNodes.clear();
                        previousBounds = currentBounds;
                        currentBounds = new double[4];
                    }
                    currentID = Long.parseLong(element.getAttribute("ref"));
                    currentRole = element.getAttribute("role");
                    break;

                case "way":
                    if (isAMemberOfARelation) {
                        isAMemberOfARelation = false;
                    }
                    if (isFirst) {
                        isFirst = false;
                    }
                    else {
                        finishedParsingObject = true;
                        previousID = currentID;
                        previousRole = currentRole;
                        previousName = currentName;
                        currentName = "";
                        previousType = currentType;
                        currentType = "";
                        previousLayer = currentLayer;
                        currentLayer = 0;
                        previousSpeedLimit = currentSpeedLimit;
                        currentSpeedLimit = 0;
                        previousIsOneWay = currentIsOneWay;
                        currentIsOneWay = false;
                        previousNodes = new ArrayList<>(currentNodes);
                        currentNodes.clear();
                        previousBounds = currentBounds;
                        currentBounds = new double[4];
                    }
                    currentID = Long.parseLong(element.getAttribute("id"));
                    currentRole = "";
                    break;

                // Bounds encountered
                case "bounds":
                    currentBounds[0] = Double.parseDouble(element.getAttribute("minlat"));
                    currentBounds[1] = Double.parseDouble(element.getAttribute("minlon"));
                    currentBounds[2] = Double.parseDouble(element.getAttribute("maxlat"));
                    currentBounds[3] = Double.parseDouble(element.getAttribute("maxlon"));
                    break;

                // New node encountered
                case "nd":
                    long ref;
                    if (element.getAttribute("ref").isEmpty())  // Is a node inside of a relation
                        ref = currentID;
                    else                                              // Is in a normal way
                        ref = Long.parseLong(element.getAttribute("ref"));

                    Node node = new Node(ref,
                            Double.parseDouble(element.getAttribute("lon")),
                            Double.parseDouble(element.getAttribute("lat")));
                    currentNodes.add(node);
                    break;

                // New tag encountered
                case "tag":
                    String key = element.getAttribute("k");

                    // If it is the tag "name"
                    if (key.equals("name")) {
                        currentName = element.getAttribute("v");
                    }

                    // If it is the tag "layer"
                    else if (key.equals("layer")) {
                        String[] layers = element.getAttribute("v").split(";");
                        currentLayer = Double.parseDouble(layers[0]);
                    }

                    // If it is the tag "maxspeed"
                    else if (key.equals("maxspeed")) {
                        String speedLimit = element.getAttribute("v").split(";")[0];
                        if (!speedLimit.equals("none")) {
                            try {
                                currentSpeedLimit = Integer.parseInt(speedLimit);
                            } catch (NumberFormatException e) {
                                String[] splittedSpeedLimit = speedLimit.split(" ");

                                if (splittedSpeedLimit[1].equals("mph")) {
                                    currentSpeedLimit = (int) (Integer.parseInt(splittedSpeedLimit[0]) * 1.609);
                                }
                                else {
                                    currentSpeedLimit = Integer.parseInt(splittedSpeedLimit[0]);
                                }
                            }
                        }
                    }

                    // If it is the tag "oneway"
                    else if (key.equals("oneway")) {
                        String isOneWay = element.getAttribute("v");
                        if (isOneWay.equals("yes")) {
                            currentIsOneWay = true;
                        }
                    }

                    // If it is the tag "junction" (mainly roundabouts), treat the road as a one way road
                    else if (key.equals("junction")) {
                        String value = element.getAttribute("v");
                        if (value.equals("roundabout")) {
                            currentIsOneWay = true;
                        }
                    }

                    // If it is one of the important tags (road/park/water/building/interest point type)
                    else if (importantTags.contains(key)) {
                        currentType = element.getAttribute("v").split(";")[0];
                    }
                    break;
            }

            if (finishedParsingObject) {
                Drawable drawable = createSpecificDrawable(typeToParse, previousID, previousNodes, previousName, previousType, previousRole, previousLayer, previousSpeedLimit, previousIsOneWay, previousBounds);
                if (drawable != null) {
                    if (isAMemberOfARelation || finishedParsingRelation) {
                        currentRelationMembers.add(drawable);
                    }
                    else {
                        data.add(drawable);
                    }
                    finishedParsingObject = false;
                }
            }

            if (finishedParsingRelation) {
                for (Drawable drawable : currentRelationMembers) {
                    drawable.setName(currentName);
                    drawable.setType(currentType);
                    drawable.setLayer(currentLayer);
                }
                data.addAll(currentRelationMembers);
                finishedParsingRelation = false;
                currentRelationMembers.clear();
                previousName = currentName;
                currentName = "";
                previousType = currentType;
                currentType = "";
                previousLayer = currentLayer;
                currentLayer = 0;
                previousSpeedLimit = currentSpeedLimit;
                currentSpeedLimit = 0;
                previousIsOneWay = currentIsOneWay;
                currentIsOneWay = false;
                previousNodes = new ArrayList<>(currentNodes);
                currentNodes.clear();
                previousBounds = currentBounds;
                currentBounds = new double[4];
            }

            // If it was the last element of the file
            if (i == entriesLength - 1) {
                Drawable drawable = createSpecificDrawable(typeToParse, currentID, currentNodes, currentName, currentType, currentRole, currentLayer, currentSpeedLimit, currentIsOneWay, currentBounds);
                if (drawable != null) {
                    if (isAMemberOfARelation) {
                        currentRelationMembers.add(drawable);
                        for (Drawable d : currentRelationMembers) {
                            d.setName(currentName);
                            d.setType(currentType);
                            d.setLayer(currentLayer);
                        }
                        data.addAll(currentRelationMembers);
                    }
                    else {
                        data.add(drawable);
                    }
                }
            }
        }

        return data;
    }

    private static Drawable createSpecificDrawable(String typeToParse, long id, List<Node> nodes, String name, String type, String role, double layer, int speedLimit, boolean isOneWay, double[] bounds) {
        switch (typeToParse) {
            case "roads":
                return createSpecificRoad(id, nodes, name, type, layer, speedLimit, isOneWay, bounds);

            case "parks":
                return new Park(id, nodes, name, type);

            case "water":
                return createSpecificWaterPlace(id, nodes, name, type, role);

            case "buildings":
                return new Building(id, nodes, name, type);

            case "landuse":
                Landuse landuse = new Landuse(id, nodes, name, type);

                // Check if the landuse type is recognized by the Landuse class, if not ignore it
                if (landuse.toIgnore) {
                    return null;
                }
                else {
                    return landuse;
                }

            case "railways":
                return new Railway(id, nodes, name, type);
        }
        return null;
    }

    static Road createSpecificRoad(long id, List<Node> nodes, String name, String type, double layer, int speedLimit, boolean isOneWay, double[] bounds) {
        switch (type) {
            case "motorway":
            case "motorway_link":
                if (speedLimit == 0) {
                    return new RoadMotorway(id, nodes, name, type, layer, DrawableMetadata.motorwayPriority, SpeedLimit.motorwayLimit, isOneWay, bounds);
                }
                else {
                    return new RoadMotorway(id, nodes, name, type, layer, DrawableMetadata.motorwayPriority, speedLimit, isOneWay, bounds);
                }

            case "trunk":
            case "trunk_link":
                if (speedLimit == 0) {
                    return new RoadTrunk(id, nodes, name, type, layer, DrawableMetadata.trunkPriority, SpeedLimit.trunkLimit, isOneWay, bounds);
                }
                else {
                    return new RoadTrunk(id, nodes, name, type, layer, DrawableMetadata.trunkPriority, speedLimit, isOneWay, bounds);
                }

            case "primary":
            case "primary_link":
                if (speedLimit == 0) {
                    return new RoadPrimary(id, nodes, name, type, layer, DrawableMetadata.primaryPriority, SpeedLimit.primarySpeedLimit, isOneWay, bounds);
                }
                else {
                    return new RoadPrimary(id, nodes, name, type, layer, DrawableMetadata.primaryPriority, speedLimit, isOneWay, bounds);
                }

            case "secondary":
            case "secondary_link":
                if (speedLimit == 0) {
                    return new RoadSecondary(id, nodes, name, type, layer, DrawableMetadata.secondaryPriority, SpeedLimit.secondarySpeedLimit, isOneWay, bounds);
                }
                else {
                    return new RoadSecondary(id, nodes, name, type, layer, DrawableMetadata.secondaryPriority, speedLimit, isOneWay, bounds);
                }

            case "tertiary":
            case "tertiary_link":
                if (speedLimit == 0) {
                    return new RoadTertiary(id, nodes, name, type, layer, DrawableMetadata.tertiaryPriority, SpeedLimit.tertiarySpeedLimit, isOneWay, bounds);
                }
                else {
                    return new RoadTertiary(id, nodes, name, type, layer, DrawableMetadata.tertiaryPriority, speedLimit, isOneWay, bounds);
                }

            case "living_street":
                if (speedLimit == 0) {
                    return new RoadLivingStreet(id, nodes, name, type, layer, DrawableMetadata.livingStreetPriority, SpeedLimit.livingStreetSpeedLimit, isOneWay, bounds);
                }
                else {
                    return new RoadLivingStreet(id, nodes, name, type, layer, DrawableMetadata.livingStreetPriority, speedLimit, isOneWay, bounds);
                }

            case "pedestrian":
                if (speedLimit == 0) {
                    return new RoadPedestrian(id, nodes, name, type, layer, DrawableMetadata.pedestrianPriority, SpeedLimit.pedestrianSpeedLimit, isOneWay, bounds);
                }
                else {
                    return new RoadPedestrian(id, nodes, name, type, layer, DrawableMetadata.pedestrianPriority, speedLimit, isOneWay, bounds);
                }

            case "residential":
            case "unclassified":
                if (speedLimit ==  0) {
                    return new RoadResidential(id, nodes, name, type, layer, DrawableMetadata.residentialPriority, SpeedLimit.residentialSpeedLimit, isOneWay, bounds);
                }
                else {
                    return new RoadResidential(id, nodes, name, type, layer, DrawableMetadata.residentialPriority, speedLimit, isOneWay, bounds);
                }

            case "service":
            case "road":
                return new RoadService(id, nodes, name, type, layer, DrawableMetadata.servicePriority, SpeedLimit.serviceSpeedLimit, isOneWay, bounds);

            case "track":
                return new RoadTrack(id, nodes, name, type, layer, DrawableMetadata.trackPriority, SpeedLimit.trackSpeedLimit, isOneWay, bounds);

            case "cycleway":
                return new RoadCycleway(id, nodes, name, type, layer, DrawableMetadata.cyclewayPriority, SpeedLimit.cyclistLimit, isOneWay, bounds);

            case "footway":
            case "path":
            case "steps":
            case "corridor":
                return new RoadFootway(id, nodes, name, type, layer, DrawableMetadata.footwayPriority, SpeedLimit.cyclistLimit, isOneWay, bounds);

            case "bridleway":
                return new RoadBridleway(id, nodes, name, type, layer, DrawableMetadata.bridlewayPriority, SpeedLimit.cyclistLimit, isOneWay, bounds);

            default:
                return null;
        }
    }

    private static Drawable createSpecificWaterPlace(long id, List<Node> nodes, String name, String type, String role) {
        switch (type) {
            case "stream":
            case "canal":
            case "drain":
            case "ditch":
                return new DrawableWater(id, nodes, name, type);
        }
        return new FillableWater(id, nodes, name, type, role);
    }

    private static List<Drawable> subdivideRoads(List<Drawable> roads) throws InterruptedException {
        // Initialize an empty list that will contain the end result of our road network
        List<Drawable> dividedRoads = Collections.synchronizedList(new ArrayList<>());

        // Create the thread pool (list of threads)
        List<Thread> threadPool = new ArrayList<>();

        // Create the threads
        int threadPoolSize = 4;
        for (int index = 0; index < threadPoolSize; index++) {
            threadPool.add(new Thread(new RoadDivider(roads, dividedRoads, threadPoolSize, index)));
        }

        // Start the threads
        for (Thread thread : threadPool) {
            thread.start();
        }

        // Wait for each thread to finish before continuing
        for (Thread thread : threadPool) {
            thread.join();
        }

        return dividedRoads;
    }
}
