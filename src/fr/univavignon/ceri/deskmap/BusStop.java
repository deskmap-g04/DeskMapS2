package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class BusStop extends DrawableIcon {

    ArrayList<String> busLineList;
    Map<String, String> busesTime;

    BusStop(String name, ArrayList<String> busLines, Node coords, long id) {
        super(name, coords, id);

        this.name = name;
        this.busLineList = new ArrayList<>();
        this.busLineList = busLines;
        busesTime = new HashMap<>();

        initDrawingParameters(Icon.busStop, Icon.smallSize, DrawableMetadata.smallZoomThreshold);
    }
}
