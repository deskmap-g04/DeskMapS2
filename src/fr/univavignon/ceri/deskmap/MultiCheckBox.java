package fr.univavignon.ceri.deskmap;

import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.*;

import java.io.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class MultiCheckBox extends GridPane {
	
	private static MultiCheckBox ref;

	private String title = "Check the lines you want to display...";
	private static Button titleButton;
	private List<String> linesTitles;

	private static ContextMenu entries;

	private Thread requesterThread;

	private Interface relatedInterface;
	
	private boolean init = true;
	
	private static boolean empty;
	
	private static boolean allSelected;
	
	public static List<TransportLine> toDraw;
	
	public static List<CheckBox> checkboxes;


	MultiCheckBox(Interface relatedInterface) {
		super();

		empty = true;
		allSelected = false;
		setAlignment(Pos.TOP_LEFT);
		setHgap(0);
		setVgap(0);
		titleButton = new Button(title);
		titleButton.setId("lineTitle");
		titleButton.setOnAction(e -> {
			if (!entries.isShowing()) {
				Platform.runLater(() -> entries.show(this, Side.BOTTOM, 40, 510));
			}
			
			else {
				Platform.runLater(() -> entries.hide());
			}
		});
		add(titleButton, 0, 1);

		entries = new ContextMenu();
		entries.setMaxHeight(300);

		this.relatedInterface = relatedInterface;

		linesTitles = new ArrayList<>();
		toDraw = new ArrayList<>();
		checkboxes = new ArrayList<>();
		requesterThread = new Thread();
		ref = this;
	}

	public static void populateLines(String city, DataContainer DT) {
		
		if(city.equals("Angers (Pays de la Loire, France)")) {
			entries.addEventHandler(Menu.ON_SHOWING, e -> {
	            javafx.scene.Node content = entries.getSkin().getNode();
	            if (content instanceof Region) {
	                ((Region) content).setMaxHeight(200);
	                ((Region) content).setLayoutY(400);
	                ((Region) content).setLayoutX(0);
	            }
	        });
			empty = false;
			CheckBox checkAll = new CheckBox("Check all lines");
			checkboxes.add(checkAll);
			CustomMenuItem allItem = new CustomMenuItem(checkAll);
			entries.getItems().add(allItem);
			allItem.setOnAction(actionEvent -> {
				Platform.runLater(() -> entries.show(ref, Side.BOTTOM, 40, 510));
				allSelected = !allSelected;
				checkAll.setSelected(allSelected);
                for (MenuItem item : entries.getItems()){
                	for (Drawable transportLine : DT.transportLines){
                    	TransportLine tl = (TransportLine) transportLine;
                        toDraw.add(tl);
                    }
                }
                for(CheckBox cb : checkboxes) {
                	cb.setSelected(allSelected);
                }
			});
			if(BusLinesLink.linkAngersOSMDataBusLines.size() > 0) {
				Iterator it = BusLinesLink.linkAngersOSMDataBusLines.entrySet().iterator();
				while (it.hasNext()) {
			    	Map.Entry pair = (Map.Entry)it.next();
			    	//TransportLine transport = (TransportLine) tl;
					String lineName = (String) pair.getKey();
					String bsOSM = BusLinesLink.linkAngersOSMDataBusLines.get(lineName);
					String hexaColor = "";
					String type = "";
					for (Drawable transportLine : DT.transportLines){
	                    if (((TransportLine) transportLine).ref.equals(bsOSM)){
	                        hexaColor = ((TransportLine) transportLine).hexaColorCode;
	                        type = ((TransportLine) transportLine).type;
	                        break;
	                    }
	                }
					lineName = "Ligne "+lineName+"("+type+")";
					CheckBox checkLine = new CheckBox(lineName);
					checkboxes.add(checkLine);
					CustomMenuItem lineItem = new CustomMenuItem(checkLine);
					entries.getItems().add(lineItem);
					if(!hexaColor.equals("")) checkLine.setStyle("-fx-text-fill: "+hexaColor+" ;");
					
					lineItem.setOnAction(actionEvent -> {
						Platform.runLater(() -> entries.show(ref, Side.BOTTOM, 40, 510));
						checkLine.setSelected(true);
	                    for (Drawable transportLine : DT.transportLines){
	                    	TransportLine tl = (TransportLine) transportLine;
	                        if (tl.ref.equals(bsOSM)){
	                        	toDraw.add(tl);
	                        }
	                    }
					});
			    }
			}
			
			// In case it's too late and Angers API isn't working anymore
			else {
				for(int i=0 ; i<40 ; i++) {
			    	//TransportLine transport = (TransportLine) tl;
					String lineName = "Lignes "+i;
					CheckBox checkLine = new CheckBox(lineName);
					checkboxes.add(checkLine);
					CustomMenuItem lineItem = new CustomMenuItem(checkLine);
					entries.getItems().add(lineItem);	
					lineItem.setOnAction(actionEvent -> {
						checkLine.setSelected(true);
						Platform.runLater(() -> entries.show(ref, Side.BOTTOM, 40, 510));
					});
			    }
			}
		}
			
		else {
			Label alert = new Label("No bus lines available for the selected town.");
			CustomMenuItem item = new CustomMenuItem(alert);
			entries.getItems().add(item);
		}
	}
	
	public boolean isEmpty() {
		return empty;
	}
	
	public static void reset() {
		entries.hide();
		toDraw = new ArrayList();
		for(CheckBox cb : checkboxes) {
			cb.setSelected(false);
		}
		allSelected = false;
	}
}
