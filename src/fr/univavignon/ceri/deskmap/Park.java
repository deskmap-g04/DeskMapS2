package fr.univavignon.ceri.deskmap;

import java.util.List;

class Park extends Fillable {

    Park(long id, List<Node> nodes, String name, String type) {
        super(id, nodes, name, type);

        initDrawingParameters(MapColor.parkFiller, MapColor.parkFiller);
    }
}
