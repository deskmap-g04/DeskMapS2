package fr.univavignon.ceri.deskmap;

import javafx.scene.control.ContextMenu;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.ScrollEvent;

import java.util.List;

class MapHandler {

    static boolean isReady = false;

    private static int recalculatedIndex = 0;

    static int zoomLevel = 0;

    private static double zoomMultiplier = 1.3;

    private static double xCenter = Interface.canvasMap.getWidth() / 2;
    private static double yCenter = Interface.canvasMap.getHeight() / 2;

    private static double mouseXBefore;
    private static double mouseYBefore;

    private static double mouseXAfter;
    private static double mouseYAfter;

    private static double xOffsetAtZoom0 = 0;
    private static double yOffsetAtZoom0 = 0;

    private static double currentScale = 1;

    private static double crossroadVisibilityRange = 5; // Is multiplied by the crossroadVisibilityFactor for each zoom in/out
    private static double crossroadVisibilityFactor = 1.20;

    private static ContextMenu currentlyOpenedCrossroadPopup;
    private static ContextMenu currentlyOpenedBusStopPopup;


    static void activateMapEvents(DataContainer DT) {

        // Zoom in/out event
        Interface.canvasMap.setOnScroll((ScrollEvent event) -> {
            double deltaY = event.getDeltaY();
            if (deltaY > 0) {
                if (zoomLevel < 20) {
                    zoomLevel++;
                    DrawableMetadata.adaptToCurrentZoomLevel(zoomLevel);
                    zoomIn(event.getX(), event.getY());
                }
            }
            else {
                if (zoomLevel > DrawableMetadata.maxZoomThreshold) {
                    zoomLevel--;
                    DrawableMetadata.adaptToCurrentZoomLevel(zoomLevel);
                    zoomOut(event.getX(), event.getY());
                }
            }
        });

        // Click event
        Interface.canvasMap.setOnMousePressed((MouseEvent event) -> {
            hideCrossroadPopup();

            mouseXBefore = event.getX();
            mouseYBefore = event.getY();

            // Check if a crossroad or busStops was left-clicked
            if (DT.city.name.equals("Angers") && DT.city.country.equals("France")) {
                hideBusStopPopup();
                if (event.getButton() == MouseButton.SECONDARY) {
                    checkIfCrossroadWasClicked(DT, mouseXBefore, mouseYBefore, event);
                    checkIfBusStopWasClicked(DT, mouseXBefore, mouseYBefore, event);
                }
            }else{
                if (event.getButton() == MouseButton.SECONDARY) {
                    checkIfCrossroadWasClicked(DT, mouseXBefore, mouseYBefore, event);
                }
            }

        });

        // Drag event
        Interface.canvasMap.setOnMouseDragged((MouseEvent event) -> {
            mouseXAfter = event.getX();
            mouseYAfter = event.getY();

            if (event.isPrimaryButtonDown()){
                drag();
            }
        });

        // Move event
        Interface.canvasMap.setOnMouseMoved((MouseEvent event) -> refreshActiveCrossroads(DT, event.getX(), event.getY()));
    }

    static void resetMapHandler() {
        isReady = false;
        recalculatedIndex = 0;
        zoomLevel = 0;
        currentScale = 1;
        crossroadVisibilityRange = 5;

        DrawableMetadata.adaptToCurrentZoomLevel(zoomLevel);
    }

    private static void zoomIn(double mouseX, double mouseY) {
        if (isReady) {
            isReady = false;
            recalculatedIndex++;
            currentScale *= zoomMultiplier;
            MapScale.scaleInMeters /= zoomMultiplier;
            crossroadVisibilityRange *= crossroadVisibilityFactor;

            // Recalculate offset
            recalculateOffsetAfterZoom(zoomMultiplier, mouseX, mouseY);

            // Map (parks, water, buildings, roads, landuse)
            for (List<Drawable> list : Converter.map) {
                for (Drawable drawable : list) {
                    for (Node node : drawable.nodes) {
                        recalculateAfterZoom(node, zoomMultiplier, mouseX, mouseY);
                    }
                }
            }

            // Map points (buses, bus stops)
            for (List<DrawableIcon> list : Converter.refreshableMapPoints) {
                for (DrawableIcon drawableIcon : list) {
                    recalculateAfterZoom(drawableIcon.node, zoomMultiplier, mouseX, mouseY);
                }
            }

            Drawer.drawMap();
            isReady = true;
        }
    }

    private static void zoomOut(double mouseX, double mouseY) {
        if (isReady) {
            isReady = false;
            recalculatedIndex++;
            currentScale /= zoomMultiplier;
            MapScale.scaleInMeters *= zoomMultiplier;
            crossroadVisibilityRange /= crossroadVisibilityFactor;

            // Recalculate offset
            recalculateOffsetAfterZoom(1 / zoomMultiplier, mouseX, mouseY);

            // Map (parks, water, buildings, roads, landuse)
            for (List<Drawable> list : Converter.map) {
                for (Drawable drawable : list) {
                    for (Node node : drawable.nodes) {
                        recalculateAfterZoom(node, 1 / zoomMultiplier, mouseX, mouseY);
                    }
                }
            }

            // Map points (buses, intersections)
            for (List<DrawableIcon> list : Converter.refreshableMapPoints) {
                for (DrawableIcon drawableIcon : list) {
                    recalculateAfterZoom(drawableIcon.node, 1 / zoomMultiplier, mouseX, mouseY);
                }
            }

            Drawer.drawMap();
            isReady = true;
        }
    }

    private static void recalculateAfterZoom(Node node, double multiplier, double mouseX, double mouseY) {
        if (node.recalculatedIndex != recalculatedIndex) {
            node.xPos = mouseX + (node.xPos - mouseX) * multiplier;
            node.yPos = mouseY + (node.yPos - mouseY) * multiplier;
            node.recalculatedIndex = recalculatedIndex;
        }
    }

    private static void recalculateOffsetAfterZoom(double multiplier, double mouseX, double mouseY) {
        double xCenterAfter = mouseX + (xCenter - mouseX) * multiplier;
        double yCenterAfter = mouseY + (yCenter - mouseY) * multiplier;

        double xOffset = xCenterAfter - xCenter;
        double yOffset = yCenterAfter - yCenter;

        xOffsetAtZoom0 += (xOffset / currentScale);
        yOffsetAtZoom0 += (yOffset / currentScale);
    }

    private static void drag() {
        if (isReady) {
            isReady = false;
            recalculatedIndex++;

            double xTranslate = mouseXAfter - mouseXBefore;
            double yTranslate = mouseYAfter - mouseYBefore;

            xOffsetAtZoom0 += xTranslate / currentScale;
            yOffsetAtZoom0 += yTranslate / currentScale;

            // Map (parks, water, buildings, roads)
            for (List<Drawable> list : Converter.map) {
                for (Drawable drawable : list) {
                    for (Node node : drawable.nodes) {
                        recalculateAfterDrag(node, xTranslate, yTranslate);
                    }
                }
            }

            // Map points (buses, intersections)
            for (List<DrawableIcon> list : Converter.refreshableMapPoints) {
                for (DrawableIcon drawableIcon : list) {
                    recalculateAfterDrag(drawableIcon.node, xTranslate, yTranslate);
                }
            }

            Drawer.drawMap();

            mouseXBefore = mouseXAfter;
            mouseYBefore = mouseYAfter;
            isReady = true;
        }
    }

    private static void recalculateAfterDrag(Node node, double xTranslate, double yTranslate) {
        if (node.recalculatedIndex != recalculatedIndex) {
            node.xPos += xTranslate;
            node.yPos += yTranslate;
            node.recalculatedIndex = recalculatedIndex;
        }
    }

    static void adaptPositionToCurrent(Node node) {
        // Position
        node.xPos += xOffsetAtZoom0;
        node.yPos += yOffsetAtZoom0;

        // Zoom in
        if (zoomLevel > 0) {
            for (int i = 0; i < zoomLevel; i++) {
                recalculateCenteredZoomOnce(node, zoomMultiplier);
            }
        }

        // else zoom out
        else if (zoomLevel < 0) {
            for (int i = 0; i > zoomLevel; i--) {
                recalculateCenteredZoomOnce(node, 1 / zoomMultiplier);
            }
        }
    }

    private static void recalculateCenteredZoomOnce(Node node, double multiplier) {
        node.xPos = xCenter + (node.xPos - xCenter) * multiplier;
        node.yPos = yCenter + (node.yPos - yCenter) * multiplier;
    }

    private static void refreshActiveCrossroads(DataContainer DT, double currentMouseX, double currentMouseY) {
        if (isReady) {
            if (zoomLevel >= DT.crossroads.get(0).zoomLevelThreshold) {
                isReady = false;
                DT.activeCrossroads.clear();

                // Activate the crossroads that are close to the mouse
                for (Crossroad crossroad : DT.crossroads) {
                    Node node = crossroad.node;
                    double xDistanceFromMouse = Math.abs(node.xPos - currentMouseX);
                    double yDistanceFromMouse = Math.abs(node.yPos - currentMouseY);

                    if (xDistanceFromMouse <= crossroadVisibilityRange && yDistanceFromMouse <= crossroadVisibilityRange) {
                        DT.activeCrossroads.add(crossroad);
                    }
                }

                Drawer.drawMap();
            }
            else if (!DT.activeCrossroads.isEmpty()) {
                isReady = false;
                DT.crossroads.addAll(DT.activeCrossroads);
                DT.activeCrossroads.clear();
                isReady = true;
            }
        }
    }

    private static void checkIfCrossroadWasClicked(DataContainer DT, double currentMouseX, double currentMouseY, MouseEvent mouseEvent) {
        if (isReady) {
            if (!DT.activeCrossroads.isEmpty()) {
                isReady = false;

                // Check if an active crossroad is in the click range
                for (Crossroad crossroad : DT.activeCrossroads) {
                    Node node = crossroad.node;
                    double xDistanceFromMouse = Math.abs(node.xPos - currentMouseX);
                    double yDistanceFromMouse = Math.abs(node.yPos - currentMouseY);

                    if (xDistanceFromMouse <= crossroad.size / 2f && yDistanceFromMouse <= crossroad.size / 2f) {
                        currentlyOpenedCrossroadPopup = CrossroadMenu.display(crossroad, mouseEvent, DT);
                        break;
                    }
                }

                isReady = true;
            }
        }
    }


    private static void hideCrossroadPopup() {
        if (currentlyOpenedCrossroadPopup != null) {
            currentlyOpenedCrossroadPopup.hide();
            currentlyOpenedCrossroadPopup = null;
        }
    }

    private static void checkIfBusStopWasClicked(DataContainer DT, double currentMouseX, double currentMouseY, MouseEvent mouseEvent) {
        if (isReady) {
            isReady = false;

            // Check if an active crossroad is in the click range
            for ( DrawableIcon  busStop : DT.busStops) {
                BusStop b = (BusStop) busStop;
                Node node = busStop.node;
                double xDistanceFromMouse = Math.abs(node.xPos - currentMouseX);
                double yDistanceFromMouse = Math.abs(node.yPos - currentMouseY);

                if ((xDistanceFromMouse < 2 && xDistanceFromMouse > -5) && (yDistanceFromMouse < 5 && yDistanceFromMouse > -5)){
                    currentlyOpenedBusStopPopup = BusStopsMenu.display(b, mouseEvent, DT);
                    break;
                }
            }

            isReady = true;
        }
    }

    private static void hideBusStopPopup() {
        if (currentlyOpenedBusStopPopup != null) {
            currentlyOpenedBusStopPopup.hide();
            currentlyOpenedBusStopPopup = null;
        }
    }

}
