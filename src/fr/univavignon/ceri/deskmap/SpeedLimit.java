package fr.univavignon.ceri.deskmap;

class SpeedLimit {
    static int trackSpeedLimit = 15;
    static int serviceSpeedLimit = 20;
    static int pedestrianSpeedLimit = 20;
    static int livingStreetSpeedLimit = 30;
    static int residentialSpeedLimit = 40;
    static int tertiarySpeedLimit = 50;
    static int secondarySpeedLimit = 70;
    static int primarySpeedLimit = 90;
    static int trunkLimit = 110;
    static int motorwayLimit = 130;

    static int cyclistLimit = 20;
    static int pedestrianLimit = 5;


    static double getMaxSpeedInMeters(String transportMode) {
        switch (transportMode) {
            case "Pedestrian":
                return pedestrianLimit / 3.6;
            case "Bike":
                return cyclistLimit / 3.6;
            case "Car":
                return motorwayLimit / 3.6;
            default:
                return 0;
        }
    }
}
