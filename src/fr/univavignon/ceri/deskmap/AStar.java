package fr.univavignon.ceri.deskmap;

import java.util.*;
import javafx.scene.control.Label;

class AStar {

    private Crossroad initialNode;
    private Crossroad finalNode;

    private PriorityQueue<Crossroad> openList;
    private Set<Crossroad> closedSet;

    private List<Crossroad> searchList;

    private String calculationMode;
    private String transportMode;


    AStar(List<Crossroad> crossroads, Crossroad initialNode, Crossroad finalNode) {
        this.initialNode = initialNode;
        this.finalNode = finalNode;

        calculationMode = Interface.calculationMode.getValue().toString();
        Label transport = (Label) Interface.transportMode.getSelectionModel().getSelectedItem();
        transportMode = transport.getText().toString();

        openList = new PriorityQueue<>(Comparator.comparingDouble(o -> o.f));
        calculateHeuristics(crossroads);
        closedSet = new HashSet<>();
    }

    private void calculateHeuristics(List<Crossroad> crossroads) {
        searchList = crossroads;
        for (Crossroad crossroad : searchList) {
            crossroad.calculateHeuristic(finalNode, calculationMode, transportMode);
            crossroad.parent = null;
        }
    }

    List<Road> findPath() {
        openList.add(initialNode);
        while (!openList.isEmpty()) {
            Crossroad currentNode = openList.poll();
            closedSet.add(currentNode);
            if (currentNode == finalNode) {
                Interface.resetRoute();
                Interface.appendToStateLn("→ Found path from " + Interface.numDep.getText() + " " + Interface.roadNameDep.getText() + " to " + Interface.numArr.getText() + " " + Interface.roadNameArr.getText());
                return getPath(currentNode);
            }
            else {
                addAdjacentNodes(currentNode);
            }
        }
        Interface.resetRoute();
        Interface.appendToRouteLn("→ No path found from " + Interface.numDep.getText() + " " + Interface.roadNameDep.getText() + " to " + Interface.numArr.getText() + " " + Interface.roadNameArr.getText()); // Route panel
        Interface.appendToStateLn("→ No path found from " + Interface.numDep.getText() + " " + Interface.roadNameDep.getText() + " to " + Interface.numArr.getText() + " " + Interface.roadNameArr.getText()); // State bar
        return new ArrayList<>();
    }

    private List<Road> getPath(Crossroad currentNode) {
        List<Road> path = new ArrayList<>();
        Crossroad endOfRoad;
        while ( (endOfRoad = currentNode.parent) != null ) {
            path.add(0, findRoad(currentNode, endOfRoad));
            currentNode = endOfRoad;
        }
        printItinerary(path);
        return path;
    }

    private Road findRoad(Crossroad beginning, Crossroad end) {
        for (Map.Entry<Crossroad, Road> crossroadRoadEntry : beginning.neighbours.entrySet()) {
            if (crossroadRoadEntry.getKey() == end) {
                return crossroadRoadEntry.getValue();
            }
        }
        return null;
    }

    private void addAdjacentNodes(Crossroad currentNode) {
        for (Map.Entry<Crossroad, Road> crossroadRoadEntry : currentNode.neighbours.entrySet()) {
            Crossroad adjacentNode = crossroadRoadEntry.getKey();
            Road road = crossroadRoadEntry.getValue();
            double cost;

            if (calculationMode.equals("Distance")) {
                cost = road.distance;
            }
            else {
                cost = calculateTime(road);
            }

            // Check if cars or bikes are allowed on the road
            if (transportMode.equals("Car") && road.motoristsAllowed
                    || transportMode.equals("Bike") && road.cyclistsAllowed) {
                // Check if it's a one way road and if the user don't allow going in the wrong direction
                if (Interface.sense.isSelected() && road.isOneWay) {
                    Node beginningNode = road.nodes.get(0);

                    // As the nodes of the one way roads are going in the direction of the road, check if the first
                    // node of the road has the same lat/lon than the crossroad where we came from.
                    // If true: we are going on the correct direction, check the adjacent crossroad
                    // Else: we are going in the wrong direction (not allowed), don't check the adjacent crossroad
                    if (beginningNode.lat == currentNode.node.lat && beginningNode.lon == currentNode.node.lon) {
                        checkNode(currentNode, adjacentNode, cost);
                    }
                }
                else {
                    checkNode(currentNode, adjacentNode, cost);
                }
            }
            // Check if walking is allowed on the road
            else if (transportMode.equals("Pedestrian") && road.pedestriansAllowed) {
                checkNode(currentNode, adjacentNode, cost);
            }
        }
    }

    private void checkNode(Crossroad currentNode, Crossroad adjacentNode, double cost) {
        if (!closedSet.contains(adjacentNode)) {
            if (!openList.contains(adjacentNode)) {
                adjacentNode.setNodeData(currentNode, cost);
                openList.add(adjacentNode);
            }
            else {
                boolean changed = adjacentNode.checkBetterPath(currentNode, cost);
                if (changed) {
                    // Remove and Add the changed node, so that the PriorityQueue can sort again its
                    // contents with the modified "finalCost" value of the modified node
                    openList.remove(adjacentNode);
                    openList.add(adjacentNode);
                }
            }
        }
    }

    private void printItinerary(List<Road> path) {
        Interface.appendToRouteLn("→ Transport mode chosen: " + transportMode);
        Interface.appendToRouteLn("→ Optimization criteria: " + calculationMode);
        Interface.appendToRouteLn("This is the route:");

        Road traveledRoad = null;

        double traveledRoadDistance = 0;
        double totalDistance = 0;

        double traveledRoadTime = 0;
        double totalTime = 0;

        boolean haveBeenAppended = false;
        int index = 1;

        for (Road road : path) {
            haveBeenAppended = false;

            if (traveledRoad == null) {
                traveledRoad = road;
                traveledRoadDistance = road.distance;
                traveledRoadTime = calculateTime(road);
            }
            else {
                if (road.name.equals(traveledRoad.name)) {
                    traveledRoadDistance += road.distance;
                    traveledRoadTime += calculateTime(road);
                }
                else {
                    Interface.appendStepToRoute(index++, traveledRoad.name, traveledRoadDistance, traveledRoadTime);
                    haveBeenAppended = true;

                    totalDistance += traveledRoadDistance;
                    totalTime += traveledRoadTime;

                    traveledRoad = road;
                    traveledRoadDistance = road.distance;
                    traveledRoadTime = calculateTime(road);
                }
            }
        }

        if (!haveBeenAppended) {
            assert traveledRoad != null;
            Interface.appendStepToRoute(index, traveledRoad.name, traveledRoadDistance, traveledRoadTime);
            totalDistance += traveledRoadDistance;
            totalTime += traveledRoadTime;
        }

        Interface.appendToRouteLn("→ Total distance: " + Math.round(totalDistance) + "m");
        Interface.appendToRouteLn("→ Total time: " + Math.round(totalTime) + "s");
    }

    private double calculateTime(Road road) {
        if (transportMode.equals("Pedestrian")) {
            return road.distance / (SpeedLimit.pedestrianLimit / 3.6);
        }
        else if (transportMode.equals("Bike")) {
            return road.distance / (SpeedLimit.cyclistLimit / 3.6);
        }
        else if (transportMode.equals("Car")) {
            return road.distance / (road.speedLimit / 3.6);
        }
        return 0;
    }
}
