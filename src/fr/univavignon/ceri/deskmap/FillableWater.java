package fr.univavignon.ceri.deskmap;

import java.util.List;

class FillableWater extends Fillable {

    FillableWater(long id, List<Node> nodes, String name, String type, String role) {
        super(id, nodes, name, type, role);

        initDrawingParameters(MapColor.waterFiller, MapColor.waterFiller);
    }
}
