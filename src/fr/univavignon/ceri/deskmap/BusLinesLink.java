package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

class BusLinesLink {
    static ArrayList<String> busLinesListFromAngersData = new ArrayList<>();
    static ArrayList<String> busLinesListFromOSMData = new ArrayList<>();
    static Map<String, String> linkAngersOSMDataBusLines = new HashMap<>();

    /**
     * function to check if the busLine name exist in the anger-data
     * @param busLineToAdd
     * @return
     */
    static boolean checkIfExistsAndAddAngersList(String busLineToAdd){
        for (String busLine : busLinesListFromAngersData){
            if (busLine.equals(busLineToAdd)){
                return true;
            }
        }
        busLinesListFromAngersData.add(busLineToAdd);
        return false;
    }

    /**
     * function to check if the busLine name exist in the OSM-data
     * @param busLineToAdd
     * @return
     */
    static boolean checkIfExistsAndAddOSMList(String busLineToAdd){
        for (String busLine : busLinesListFromOSMData){
            if (busLine.equals(busLineToAdd)){
                return true;
            }
        }
        busLinesListFromOSMData.add(busLineToAdd);
        return false;
    }

    /**
     * function to build the map wich link the busLines names from each dataBase (anger-data and OSM-data)
     */
    static void setLinkAngersOSMDataBusLines(){
        for (String angersBus : busLinesListFromAngersData){
            if (!angersBus.equals("NUIT") && !angersBus.equals("NAVM") && !angersBus.equals("ABUS")){
                if (angersBus.equals("05")){
                    linkAngersOSMDataBusLines.put(angersBus, "5a");
                }else if (angersBus.charAt(0) == '0'){
                    linkAngersOSMDataBusLines.put(angersBus,String.valueOf(angersBus.charAt(1)));
                }else if (angersBus.charAt(1) == 'S'){
                    linkAngersOSMDataBusLines.put(angersBus, angersBus.charAt(0) + "s");
                }else if (angersBus.charAt(1) == 'D'){
                    linkAngersOSMDataBusLines.put(angersBus, angersBus.charAt(0) + "d");
                }else if (angersBus.equals("12D")){
                    linkAngersOSMDataBusLines.put(angersBus, "12d");
                }else{
                    linkAngersOSMDataBusLines.put(angersBus, angersBus);
                }
            }
        }
    }
}
