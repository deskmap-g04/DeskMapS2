package fr.univavignon.ceri.deskmap;

import javafx.application.Application;
import javafx.stage.Stage;

/**
 * This class is used to launch the software.
 *
 * @author 	CHAIBAINOU Caroline
 * @author 	PARRILLA Valentin
 * @author 	POTIN Clément
 * @author 	SASSI Nader
 */
public class Launcher extends Application {

	/**
	 * Launches the application
	 * @param args : the input 
	 */
	public static void main(String[] args) {
		System.out.println("Main method inside Thread : " + Thread.currentThread().getName());
		launch(args);
	}

    @Override
    public void start(Stage stage) {
		// Init classes
		Parser.init();

    	Interface Interface = new Interface(stage);
    	Interface.setIdsForCSS();
    	
    	stage.setMinWidth(760);
	    stage.setMinHeight(575);
		stage.setScene(Interface.scene);
		final boolean resizable = stage.isResizable();
		stage.setResizable(!resizable);
		stage.setResizable(resizable);
	    stage.setTitle("DeskMap");
	    stage.sizeToScene();
	    stage.show();
    }
}
