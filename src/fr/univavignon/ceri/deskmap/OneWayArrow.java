package fr.univavignon.ceri.deskmap;

import javafx.scene.canvas.GraphicsContext;
import javafx.scene.transform.Rotate;

class OneWayArrow extends DrawableIcon {

    int angle;

    Node previous;
    Node next;


    OneWayArrow(String name, Node coords, Node previous, Node next) {
        super(name, coords);

        this.previous = previous;
        this.next = next;

        initDrawingParameters(Icon.oneWayArrow, Icon.smallSize, DrawableMetadata.XSZoomThreshold);
    }

    @Override
    void draw() {
        // Don't draw if the "Consider direction" checkbox isn't checked
        if (!Interface.sense.isSelected()) {
            return;
        }

        if (MapHandler.zoomLevel >= zoomLevelThreshold) {
            if (node.xPos > 0 && node.xPos < Interface.canvasMap.getWidth()) {      // If the x coord is visible
                if (node.yPos > 0 && node.yPos < Interface.canvasMap.getHeight()) { // And if the y coord is visible
                    int xTopLeft = (int) (node.xPos - (iconSize / 2));
                    int yTopLeft = (int) (node.yPos - (iconSize / 2));

                    Interface.gc.setGlobalAlpha(0.3);

                    Interface.gc.save();
                    rotate(Interface.gc, angle, xTopLeft + (double)(iconSize / 2), yTopLeft + (double)(iconSize / 2));
                    Interface.gc.drawImage(icon, xTopLeft, yTopLeft, iconSize, iconSize);
                    Interface.gc.restore();

                    Interface.gc.setGlobalAlpha(1);
                }
            }
        }
    }

    private static void rotate(GraphicsContext gc, double angle, double xCenter, double yCenter) {
        Rotate r = new Rotate(angle, xCenter, yCenter);
        gc.setTransform(r.getMxx(), r.getMyx(), r.getMxy(), r.getMyy(), r.getTx(), r.getTy());
    }
}
