package fr.univavignon.ceri.deskmap;

import java.util.List;

class RoadCycleway extends DashedRoad {

    RoadCycleway(long id, List<Node> nodes, String name, String type, double layer, int priorityInLayer, int speedLimit, boolean isOneWay, double[] bounds) {
        super(id, nodes, name, type, layer, priorityInLayer, speedLimit, isOneWay, false, bounds);

        this.dashSpace = 3;

        initDrawingParameters(MapColor.cyclewayOutline, MapColor.cyclewayOutlineDisabled, DrawableMetadata.basicSize, DrawableMetadata.XSZoomThreshold);
    }
}
