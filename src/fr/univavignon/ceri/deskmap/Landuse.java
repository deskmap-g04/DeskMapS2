package fr.univavignon.ceri.deskmap;

import java.util.List;

class Landuse extends Fillable {

    boolean toIgnore = false;

    Landuse(long id, List<Node> nodes, String name, String type) {
        super(id, nodes, name, type);

        changeColors();
    }

    private void changeColors() {
        switch (type) {
            case "commercial":
                initDrawingParameters(MapColor.landuseCommercial, MapColor.landuseCommercial);
                break;
            case "construction":
                initDrawingParameters(MapColor.landuseConstruction, MapColor.landuseConstruction);
                break;
            case "industrial":
                initDrawingParameters(MapColor.landuseIndustrial, MapColor.landuseIndustrial);
                break;
            case "residential":
                initDrawingParameters(MapColor.landuseResidential, MapColor.landuseResidential);
                break;
            case "retail":
                initDrawingParameters(MapColor.landuseRetail, MapColor.landuseRetail);
                break;

            case "allotments":
                initDrawingParameters(MapColor.landuseAllotments, MapColor.landuseAllotments);
                break;
            case "farmland":
                initDrawingParameters(MapColor.landuseFarmland, MapColor.landuseFarmland);
                break;
            case "farmyard":
                initDrawingParameters(MapColor.landuseFarmyard, MapColor.landuseFarmyard);
                break;
            case "forest":
                initDrawingParameters(MapColor.landuseForest, MapColor.landuseForest);
                break;
            case "meadow":
                initDrawingParameters(MapColor.landuseMeadow, MapColor.landuseMeadow);
                break;
            case "orchard":
                initDrawingParameters(MapColor.landuseOrchard, MapColor.landuseOrchard);
                break;
            case "vineyard":
                initDrawingParameters(MapColor.landuseVineyard, MapColor.landuseVineyard);
                break;

            case "basin":
                initDrawingParameters(MapColor.landuseBasin, MapColor.landuseBasin);
                break;
            case "brownfield":
                initDrawingParameters(MapColor.landuseBrownfield, MapColor.landuseBrownfield);
                break;
            case "cemetery":
                initDrawingParameters(MapColor.landuseCemetery, MapColor.landuseCemetery);
                break;
            case "garages":
                initDrawingParameters(MapColor.landuseGarages, MapColor.landuseGarages);
                break;
            case "grass":
                initDrawingParameters(MapColor.landuseGrass, MapColor.landuseGrass);
                break;
            case "greenfield":
                initDrawingParameters(MapColor.landuseGreenfield, MapColor.landuseGreenfield);
                break;
            case "greenhouse_horticulture":
                initDrawingParameters(MapColor.landuseGreenhouseHorticulture, MapColor.landuseGreenhouseHorticulture);
                break;
            case "landfill":
                initDrawingParameters(MapColor.landuseLandfill, MapColor.landuseLandfill);
                break;
            case "military":
                initDrawingParameters(MapColor.landuseMilitary, MapColor.landuseMilitary);
                break;
            case "plant_nursery":
                initDrawingParameters(MapColor.landusePlantNursery, MapColor.landusePlantNursery);
                break;
            case "quarry":
                initDrawingParameters(MapColor.landuseQuarry, MapColor.landuseQuarry);
                break;
            case "railway":
                initDrawingParameters(MapColor.landuseRailway, MapColor.landuseRailway);
                break;
            case "recreation_ground":
                initDrawingParameters(MapColor.landuseRecreationGround, MapColor.landuseRecreationGround);
                break;
            case "religious":
                initDrawingParameters(MapColor.landuseReligious, MapColor.landuseReligious);
                break;
            case "reservoir":
                initDrawingParameters(MapColor.landuseReservoir, MapColor.landuseReservoir);
                break;
            case "village_green":
                initDrawingParameters(MapColor.landuseVillageGreen, MapColor.landuseVillageGreen);
                break;

            case "tourism":
                initDrawingParameters(MapColor.landuseTourism, MapColor.landuseTourism);
                break;
            case "community_food_growing":
                initDrawingParameters(MapColor.gray, MapColor.backgroundColor);
                break;
            case "healthcare":
                initDrawingParameters(MapColor.landuseHealthcare, MapColor.landuseHealthcare);
                break;
            case "railway_station":
                initDrawingParameters(MapColor.backgroundColor, MapColor.backgroundColor);
                break;

            default:
                toIgnore = true;
        }
    }

    @Override
    void setName(String name) {
        super.setName(name);
    }

    @Override
    void setType(String type) {
        super.setType(type);

        changeColors();
    }

    @Override
    void setLayer(Double layer) {
        super.setLayer(layer);
    }
}
