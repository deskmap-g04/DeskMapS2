package fr.univavignon.ceri.deskmap;

import java.util.List;

class DrawableWater extends Drawable {

    DrawableWater(long id, List<Node> nodes, String name, String type) {
        super(id, nodes, name, type);

        initDrawingParameters(MapColor.waterFiller, DrawableMetadata.smallSize);
    }

    DrawableWater(long id, List<Node> nodes, String name, String type, double layer) {
        super(id, nodes, name, type, layer);

        initDrawingParameters(MapColor.waterFiller, DrawableMetadata.smallSize);
    }
}
