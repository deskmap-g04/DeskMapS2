package fr.univavignon.ceri.deskmap;

import javafx.scene.shape.StrokeLineCap;

import java.util.List;

class Drawer {

    static void drawMap() {
        Interface.gc.clearRect(0, 0, Interface.canvasMap.getWidth(), Interface.canvasMap.getHeight());
        Interface.gc.setLineCap(StrokeLineCap.ROUND);

        // Draw the map (parks, water, buildings, roads, landuse, bus lines (if they exist))
        for (List<Drawable> list : Converter.map) {
            drawDrawableList(list);
        }

        // Draw the map points (buses, bus stops, one-way arrows)
        Interface.gc.setLineWidth(1);
        for (List<DrawableIcon> list : Converter.refreshableMapPoints) {
            if (list != null) {
                drawDrawableIconList(list);
            }
        }

        // Draw the itinerary if it exists
        Converter.itinerary.draw();

        // Draw the one-way arrows
        for (List<DrawableIcon> list : Converter.otherMapPoints) {
            if (list != null) {
                drawDrawableIconList(list);
            }
        }

        // Draw the crossroads
        drawCrossroads(Converter.mapCrossroads);

        // Draw the departure and arrival points if they exist
        if (Converter.itinerary != null && Converter.itinerary.departure != null) {
            Converter.itinerary.departure.draw();
        }
        if (Converter.itinerary != null && Converter.itinerary.arrival != null) {
            Converter.itinerary.arrival.draw();
        }

        // Draw the map scale
        MapScale.draw();

        MapHandler.isReady = true;
    }

    private static void drawDrawableList(List<Drawable> drawables) {
        for (Drawable drawable : drawables) {
            drawable.draw();
        }
    }

    private static void drawDrawableIconList(List<DrawableIcon> drawableIcons) {
        for (DrawableIcon drawableIcon : drawableIcons) {
            drawableIcon.draw();
        }
    }

    private static void drawCrossroads(List<Crossroad> crossroads) {
        for (Crossroad crossroad : crossroads) {
            crossroad.draw();
        }
    }
}
