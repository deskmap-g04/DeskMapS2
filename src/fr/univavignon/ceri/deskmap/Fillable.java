package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;

import java.util.List;

abstract class Fillable extends Drawable {

    Color fillerColor = MapColor.white;


    Fillable(long id, List<Node> nodes, String name, String type) {
        super(id, nodes, name, type);
    }

    Fillable(long id, List<Node> nodes, String name, String type, String role) {
        super(id, nodes, name, type, role);
    }

    void initDrawingParameters(Color outlineColor, Color fillerColor) {
        if (role.equals("inner")) {
            this.outlineColor = MapColor.backgroundColor;
            this.fillerColor = MapColor.backgroundColor;
        }
        else {
            this.outlineColor = outlineColor;
            this.fillerColor = fillerColor;
        }
    }

    void initDrawingParameters(Color outlineColor, Color fillerColor, int zoomLevelThreshold) {
        if (role.equals("inner")) {
            this.outlineColor = MapColor.backgroundColor;
            this.fillerColor = MapColor.backgroundColor;
        }
        else {
            this.outlineColor = outlineColor;
            this.fillerColor = fillerColor;
            this.zoomLevelThreshold = zoomLevelThreshold;
        }
    }

    void draw() {
        if (MapHandler.zoomLevel >= zoomLevelThreshold) {
            if (nodes.size() > 1 && checkIfIsInBbox()) {
                Interface.gc.setLineWidth(size.get());
                Interface.gc.setStroke(outlineColor);
                Interface.gc.setFill(fillerColor);

                Node firstNode = nodes.get(0);
                Interface.gc.moveTo(firstNode.xPos, firstNode.yPos);

                Interface.gc.beginPath();
                for (Node node : nodes) {
                    Interface.gc.lineTo(node.xPos, node.yPos);
                }
                Interface.gc.lineTo(firstNode.xPos, firstNode.yPos);
                Interface.gc.closePath();

                Interface.gc.stroke();
                Interface.gc.fill();
            }
        }
    }

    @Override
    void setName(String name) {
        super.setName(name);
    }

    @Override
    void setType(String type) {
        super.setType(type);
    }

    @Override
    void setLayer(Double layer) {
        super.setLayer(layer);
    }
}
