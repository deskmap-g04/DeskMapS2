package fr.univavignon.ceri.deskmap;

import javafx.scene.shape.StrokeLineCap;

import java.util.List;

abstract class DashedRoad extends Road {

    double dashSpace = 5;

    DashedRoad(long id, List<Node> nodes, String name, String type, double layer, int priorityInLayer, int speedLimit, boolean isOneWay, boolean motoristsAllowed, double[] bounds) {
        super(id, nodes, name, type, layer, priorityInLayer, speedLimit, isOneWay, motoristsAllowed, bounds);
    }

    @Override
    void draw() {

        // Draw the dashes
        Interface.gc.setLineDashes(dashSpace);
        Interface.gc.setLineCap(StrokeLineCap.BUTT);

        super.draw();

        Interface.gc.setLineDashes(0);
        Interface.gc.setLineCap(StrokeLineCap.ROUND);
    }
}
