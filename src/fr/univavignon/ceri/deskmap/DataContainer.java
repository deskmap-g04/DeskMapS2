package fr.univavignon.ceri.deskmap;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

// The data contained here will serializable (for the save/load a map system)
class DataContainer implements Serializable {
    City city;

    List<Drawable> roads;
    List<Drawable> parks;
    List<Drawable> water;
    List<Drawable> buildings;
    List<Drawable> landuse;
    List<Drawable> railways;
    List<Drawable> transportLines;

    List<DrawableIcon> oneWayArrows;
    List<DrawableIcon> busStops;
    List<DrawableIcon> buses;

    List<Crossroad> crossroads = new ArrayList<>();
    List<Crossroad> activeCrossroads = new ArrayList<>();

    Itinerary itinerary = new Itinerary();


    DataContainer(City city) {
        this.city = city;
    }
}
