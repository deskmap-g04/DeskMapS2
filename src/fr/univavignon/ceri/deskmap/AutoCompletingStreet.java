package fr.univavignon.ceri.deskmap;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.CustomMenuItem;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.LinkedList;
import java.util.List;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Side;

/**
 * @author CHAIBAINOU Caroline
 * inspired by floralvikings' AutoComplete class https://gist.github.com/floralvikings/10290131
 *
 */
public class AutoCompletingStreet extends TextField {
	/**
	 * Trie containing all the suggestions we'll get beginning with an upper case
	 */
	private Trie suggests;
	/**
	 * Trie containing all the suggestions we'll get beginning with an lower case
	 */
	private Trie suggestsWithLowerCase;
	/**
	 * ContextMenu that will appear when for a field
	 */
	private ContextMenu entries;
	/**
	 * BufferedReader used when we need to read a file to complete the list of suggestions
	 * Set as a property so we can use it in different contexts
	 */
	private BufferedReader br;

	/**
	 * First constructor taking a List that will be used to instance path fields
	 * (different from the constructor using a String which is used to instance the
	 * town TextField
	 * 
	 * @param list : list containing all the streets for the chosen city
	 */
	AutoCompletingStreet(List<String> list) {
		super();
		this.entries = new ContextMenu();
		textProperty().addListener((observable, oldValue, newValue) -> {
			String enteredText = getText();
			if (enteredText == null || enteredText.isEmpty()) {
				this.entries.hide();
			}
			else {
				this.suggests = new Trie(new TrieNode(Character.toUpperCase(enteredText.charAt(0)))); // we instance the Trie with a first node
																												   // that is the first letter of the city
																												   // entered by the user
				for (String roadName : list) {
					if (roadName.charAt(0) == Character.toUpperCase(enteredText.charAt(0))) {
						this.suggests.insert(roadName);
						/*String roadNameLower = Character.toLowerCase(roadName.charAt(0)) + roadName.substring(0, roadName.length());
						this.suggestsWithLowerCase.insert(roadNameLower);*/
					}
				}
			}
			if (!this.suggests.isEmpty()) {
				assert enteredText != null;
				List<String> entriesList = this.suggests.autocomplete(enteredText);
				populatePopup(entriesList);
				if (!AutoCompletingStreet.this.entries.isShowing()) {
					AutoCompletingStreet.this.entries.show(AutoCompletingStreet.this, Side.BOTTOM, 0, 0);
				}
				if(entriesList.size() == 1) this.setText(entriesList.get(0));
			}
			else {
				this.entries.hide();
			}
		});
	}

	/**
	 * function called by the other populatePopup function 
	 * used to set the selected value to the TextField and to hide the other suggestions once we did so
	 * @param searchResult : 
	 */
	private void populatePopup(List<String> searchResult) {
		List<CustomMenuItem> menuItems = new LinkedList<>();
		int maxEntries = 10;
		int count = Math.min(searchResult.size(), maxEntries);
		for (int i = 0; i < count; i++) {
			final String result = searchResult.get(i);
			Label entryLabel = new Label(result);
			CustomMenuItem item = new CustomMenuItem(entryLabel, true);
			item.setOnAction(actionEvent -> {
				setText(result);
				AutoCompletingStreet.this.entries.hide();
			});
			menuItems.add(item);
		}
		AutoCompletingStreet.this.entries.getItems().clear();
		AutoCompletingStreet.this.entries.getItems().addAll(menuItems);
	}

	/**
	 * @return true if we can find the input value in the suggestions, false otherwise
	 */
	protected boolean isValid() {
		return (this.suggests.search(this.getText()));
	}
}
