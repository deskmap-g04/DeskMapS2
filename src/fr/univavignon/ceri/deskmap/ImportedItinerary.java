package fr.univavignon.ceri.deskmap;

import java.util.List;

public class ImportedItinerary {
	//List of all nodes, departure and arrival.
	private List<Node> itinerary = null;
	private Crossroad arrival = null;
	private Crossroad departure = null;
	
	ImportedItinerary(Crossroad departure, Crossroad arrival,  List<Node> itinerary) 
	{
		this.departure = departure;
		this.arrival = arrival;
		this.itinerary = itinerary;
	}
	
	public String toString() 
	{
		System.out.println("Departure : "+departure.toString()+ " Arrival : "+arrival.toString());
		System.out.println("Itinerary : ");
	    for(fr.univavignon.ceri.deskmap.Node n : itinerary)
        {
	    	System.out.println("Id : "+n.id+" lon/lat" +n.lon+"/"+n.lat);
        }
		return null;
	}
}
