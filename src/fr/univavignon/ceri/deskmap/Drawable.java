package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;

import java.util.List;

abstract class Drawable {

    long id;

    List<Node> nodes;

    Color outlineColor = MapColor.black;
    Size size = DrawableMetadata.basicSize;

    int zoomLevelThreshold = DrawableMetadata.maxZoomThreshold;

    String name;
    String type;
    String role = ""; // Can be "inner" or "outer" for members of a relation

    double layer = 0;
    int priorityInLayer = 0;


    Drawable(long id, List<Node> nodes, String name, String type) {
        this.id = id;
        this.nodes = nodes;
        this.name = name;
        this.type = type;
    }

    Drawable(long id, List<Node> nodes, String name, String type, String role) {
        this(id, nodes, name, type);

        this.role = role;
    }

    Drawable(long id, List<Node> nodes, String name, String type, double layer) {
        this(id, nodes, name, type);

        this.layer = layer;
    }

    Drawable(long id, List<Node> nodes, String name, String type, double layer, int priorityInLayer) {
        this(id, nodes, name, type, layer);

        this.priorityInLayer = priorityInLayer;
    }

    Drawable(long id, List<Node> nodes, String name, String type, String role, double layer) {
        this(id, nodes, name, type, role);

        this.layer = layer;
    }

    Drawable(long id, List<Node> nodes, String name, String type, String role, double layer, int priorityInLayer) {
        this(id, nodes, name, type, role, layer);

        this.priorityInLayer = priorityInLayer;
    }

    void initDrawingParameters(Color color, Size size) {
        outlineColor = color;
        this.size = size;
    }

    void initDrawingParameters(Color outlineColor, Size size, int zoomLevelThreshold) {
        this.outlineColor = outlineColor;
        this.size = size;
        this.zoomLevelThreshold = zoomLevelThreshold;
    }

    void draw() {
        if (MapHandler.zoomLevel >= zoomLevelThreshold) {
            if (nodes.size() > 1 && checkIfIsInBbox()) {
                Interface.gc.setLineWidth(size.get());
                Interface.gc.setStroke(outlineColor);

                Node firstNode = nodes.get(0);
                Interface.gc.moveTo(firstNode.xPos, firstNode.yPos);

                Interface.gc.beginPath();
                for (Node node : nodes) {
                    Interface.gc.lineTo(node.xPos, node.yPos);
                }

                Interface.gc.stroke();
            }
        }
    }

    boolean checkIfIsInBbox() {
        for (Node node : nodes) {
            if (node.xPos > 0 && node.xPos < Interface.canvasMap.getWidth()) {      // If the x coord is visible
                if (node.yPos > 0 && node.yPos < Interface.canvasMap.getHeight()) { // And if the y coord is visible
                    return true;                                                    // Then the entire object should be drawn
                }
            }
        }
        return false;                                                               // Not otherwise
    }

    void setName(String name) {
        this.name = name;
    }

    void setType(String type) {
        this.type = type;
    }

    void setLayer(Double layer) {
        this.layer = layer;
    }
}
