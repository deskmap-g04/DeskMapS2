package fr.univavignon.ceri.deskmap;

import java.util.List;

class RoadPrimary extends Road {

    RoadPrimary(long id, List<Node> nodes, String name, String type, double layer, int priorityInLayer, int speedLimit, boolean isOneWay, double[] bounds) {
        super(id, nodes, name, type, layer, priorityInLayer, speedLimit, isOneWay, true, bounds);

        initDrawingParameters(MapColor.primaryOutline, MapColor.primaryOutlineDisabled, DrawableMetadata.mediumSize, DrawableMetadata.maxZoomThreshold);
    }
}
