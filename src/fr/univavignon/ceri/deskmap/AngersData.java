package fr.univavignon.ceri.deskmap;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

import org.json.*;

class AngersData {

    private static final String ANGERS_API_URL_BUS = "https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-position-tr";

    private static final String ANGERS_API_URL_BUS_STOP = "https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-topologie-dessertes";

    private static final String ANGERS_API_URL_BUS_STOP_Time = "https://data.angers.fr/api/records/1.0/search/?dataset=bus-tram-circulation-passages";

    /**
     * function to build the request to get buses from anger-data
     * @return
     */
    private static String sendRequestBuses() {
        String args = "&facet=novh"
                + "&facet=mnemoligne"
                + "&facet=nomligne"
                + "&facet=dest"
                + "&rows=1000";

        return getUrlContents(ANGERS_API_URL_BUS + args);
    }

    /**
     * function to build the request to get busStops from anger-data
     * @return
     */
    private static String sendRequestBusStops() {
        String args = "&facet=ligne"
                + "&facet=mnemoligne"
                + "&facet=nomligne"
                + "&facet=dest"
                + "&facet=mnemoarret"
                + "&facet=nomarret"
                + "&facet=numarret"
                + "facet=codeparcours"
                + "&rows=10000"
                + "&sort=numarret%2Cmnemoligne";

        return getUrlContents(ANGERS_API_URL_BUS_STOP + args);
    }

    /**
     * function to build the request to get busStops passage time from anger-data
     * @return
     */
    private static String sendRequestBusStopTime() {
        String args = "&rows=10000"
                + "&facet=mnemoligne"
                + "&facet=nomligne"
                + "&facet=dest"
                + "&facet=mnemoarret"
                + "&facet=nomarret"
                + "&facet=numarret";

        return getUrlContents(ANGERS_API_URL_BUS_STOP_Time + args);
    }

    /**
     * fucntion to add passages time from the received request to the busStops
     * @param busStops list of busStops
     */
    static void getBusStopTime(List<DrawableIcon> busStops) {
        String output = sendRequestBusStopTime();
        JSONObject obj = new JSONObject(output);

        JSONArray records = obj.getJSONArray("records");
        for (int i = 0; i < records.length(); i++) {
            String passageTime = records.getJSONObject(i).getJSONObject("fields").getString("departtheorique");
            long ID = records.getJSONObject(i).getJSONObject("fields").getLong("numarret");
            String busLine = records.getJSONObject(i).getJSONObject("fields").getString("mnemoligne");

            for (int j = 0; j < busStops.size(); j++) {
                if (busStops.get(j).id == ID) {
                    ((BusStop) busStops.get(j)).busesTime.put(busLine, passageTime);
                }
            }
        }
    }

    /**
     * fucntion to get data from the received request for busStops
     * @return List of busStops
     */
    static List getBusStop() {
        List<BusStop> busStops = new ArrayList<>();

        try {
            String output = sendRequestBusStops();
            JSONObject obj = new JSONObject(output);

            JSONArray records = obj.getJSONArray("records");

            String lastBusStopName = "";
            String lastBusLine = "";
            long lastBusStopID = 0;
            Node lastCoords = new Node(0, 0);
            ArrayList<String> busLinesList = new ArrayList<>();
            for (int i = 0; i < records.length(); i++) {
                JSONObject fields = records.getJSONObject(i).getJSONObject("fields");
                String name = fields.getString("nomarret");
                String busLine = fields.getString("mnemoligne");
                long ID = fields.getLong("numarret");

                JSONArray coordinates = fields.getJSONArray("coordonnees");
                double latitude = Double.parseDouble(coordinates.get(0).toString());
                double longitude = Double.parseDouble(coordinates.get(1).toString());
                Node busStopCoordinates = new Node(longitude, latitude);

                if (i == 0) {
                    lastBusStopName = name;
                    lastBusStopID = ID;
                    lastBusLine = busLine;
                    busLinesList.add(busLine);
                    BusLinesLink.checkIfExistsAndAddAngersList(busLine);
                    lastCoords = busStopCoordinates;
                } else if (i == records.length() - 1) {
                    busStops.add(new BusStop(lastBusStopName, busLinesList, lastCoords, lastBusStopID));
                } else {
                    if (lastBusStopID == ID) {
                        if (!lastBusLine.equals(busLine)) {
                            lastBusLine = busLine;
                            busLinesList.add(busLine);
                            BusLinesLink.checkIfExistsAndAddAngersList(busLine);
                        }
                    } else {
                        busStops.add(new BusStop(lastBusStopName, busLinesList, lastCoords, lastBusStopID));
                        lastBusStopName = name;
                        lastBusStopID = ID;
                        lastBusLine = busLine;
                        busLinesList = new ArrayList<>();
                        busLinesList.add(busLine);
                        lastCoords = busStopCoordinates;
                    }
                }
            }
        } catch (org.json.JSONException e) {
            // Happens after 00:00 (Angers API related)
        }
        return busStops;
    }


    /**
     * fucntion to get data from the received request for Live Buses
     * @return
     */
    static List getLiveBus() {
        List<Bus> busLive = new ArrayList<>();

        String output = sendRequestBuses();
        JSONObject obj = new JSONObject(output);

        JSONArray records = obj.getJSONArray("records");
        for (int i = 0; i < records.length(); i++) {
            String name = records.getJSONObject(i).getJSONObject("fields").getString("nomligne");
            String destination = records.getJSONObject(i).getJSONObject("fields").getString("dest");
            JSONArray coordinates = records.getJSONObject(i).getJSONObject("fields").getJSONArray("coordonnees");
            double latitude = Double.parseDouble(coordinates.get(0).toString());
            double longitude = Double.parseDouble(coordinates.get(1).toString());
            Node BusCoordinates = new Node(longitude, latitude);
            busLive.add(new Bus(name, destination, BusCoordinates));
        }

        return busLive;
    }

    /**
     * fucntion get the Json from a request
     * @param theUrl
     * @return the received Json
     */

    private static String getUrlContents(String theUrl) {
        StringBuilder content = new StringBuilder();

        try {
            URL url = new URL(theUrl);
            URLConnection urlConnection = url.openConnection();
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                content.append(line + "\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        return content.toString();
    }
}




