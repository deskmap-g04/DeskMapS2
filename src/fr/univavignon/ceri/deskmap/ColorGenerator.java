package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;

import java.util.Random;

public class ColorGenerator {

    static private int r;
    static private int g;
    static private int b;

    static Color generateRandomTransportLineColor() {
        int min = 0;
        int max = 0;
        int sum = 0;

        while (sum < 255 || min > max - 100) {
            randomizeRGB();

            min = Math.min(r, g);
            min = Math.min(min, b);
            max = Math.max(r, g);
            max = Math.max(max, b);
            sum = r + g + b;
        }

        return Color.rgb(r, g, b);
    }

    static void randomizeRGB() {
        Random random = new Random();
        r = random.nextInt(256);
        g = random.nextInt(256);
        b = random.nextInt(256);
    }
}
