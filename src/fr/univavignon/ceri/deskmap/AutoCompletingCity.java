package fr.univavignon.ceri.deskmap;

import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.geometry.Side;
import javafx.scene.control.*;
import javafx.scene.layout.GridPane;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

class AutoCompletingCity extends GridPane {

	TextField value;
	private String cityDetails = "Angers (Pays de la Loire, France)";
	City selectedCity;
	private List<String> cityDetailsSuggestions;

	private ContextMenu entries;
	private List<ContextMenu> oldEntries;

	private Button arrowButton;

	private Thread requesterThread;

	private Interface relatedInterface;

	private boolean init = true;


	AutoCompletingCity(String type, Interface relatedInterface) {
		super();

		setAlignment(Pos.TOP_LEFT);
		setHgap(0);
		setVgap(0);
		value = new TextField(cityDetails);
		value.setId("citiesValue");
		arrowButton = new Button("↓");
		add(value, 0, 1);
		add(arrowButton, 1, 1);

		entries = new ContextMenu();
		oldEntries = new ArrayList<>();

		this.relatedInterface = relatedInterface;

		if (type.equals("city")) {
			cityDetailsSuggestions = new ArrayList<>();
			requesterThread = new Thread();

			refreshList(cityDetails);
		}
	}

	void refreshList(String cityDetails) {
		this.cityDetails = cityDetails;

		relatedInterface.okButton.setDisable(true);
		oldEntries.add(entries);

		if (isValid(cityDetails)) {
			selectIt();
		}
		else {
			if (cityDetails.length() >= 3) {

				Runnable task = () -> {
					try {
						rebuildSuggestionList(cityDetails);
					} catch (IOException e) {
						e.printStackTrace();
					}

					if (init) {
						init = false;
						Platform.runLater(() -> entries.hide());
					}
				};

				if (requesterThread.isAlive())
                    requesterThread.interrupt();
				requesterThread = new Thread(task);
                requesterThread.start();
			}
			else {
				CityDetailsAutoCompleter.suggestions = new ArrayList<>();
				cityDetailsSuggestions = new ArrayList<>();
				oldEntries.add(entries);
				entries = new ContextMenu();
				hideAndDeleteOldEntries();
				Platform.runLater(() -> entries.hide());
			}
			if(this.cityDetailsSuggestions.size() == 1) {
				selectIt();
			}
		}
	}

	private void rebuildSuggestionList(String cityDetails) throws IOException {
		CityDetailsAutoCompleter.generateCitySuggestionsFrom(cityDetails);

		cityDetailsSuggestions = new ArrayList<>();
		for (City city : CityDetailsAutoCompleter.suggestions) {
			cityDetailsSuggestions.add(city.toString());
		}

		hideAndDeleteOldEntries();
		oldEntries.add(entries);
		entries = new ContextMenu();

		if (cityDetails != null && !cityDetails.isEmpty()) {

			populatePopup(CityDetailsAutoCompleter.suggestions);
			if (!entries.isShowing()) {
				Platform.runLater(() -> entries.show(this, Side.BOTTOM, 0, 0));
			}

			if (!CityDetailsAutoCompleter.suggestions.isEmpty()) {
				this.arrowButton.setOnAction(e -> {
					if (entries.isShowing()) {
						hideAndDeleteOldEntries();
						Platform.runLater(() -> entries.hide());
					}
					else {
						Platform.runLater(() -> entries.show(this, Side.BOTTOM, 0, 0));
					}
				});
			}
		}

		if (isValid(cityDetails)) {
			selectIt();
		}
	}

	private void populatePopup(List<City> searchResult) {
		int maxEntries = 10;

		int count = Math.min(searchResult.size(), maxEntries);
		for (int i = 0; i < count; i++) {
			final City currentCity = searchResult.get(i);

			final String finalResult = currentCity.toString();

			Label entryLabel = new Label(finalResult);
			CustomMenuItem item = new CustomMenuItem(entryLabel, true);

			item.setOnAction(actionEvent -> {
				value.setText(finalResult);
				hideAndDeleteOldEntries();
				Platform.runLater(() -> entries.hide());
			});

			entries.getItems().add(item);
		}
	}

	boolean isValid(String cityDetails) {
		return !cityDetailsSuggestions.isEmpty() && cityDetailsSuggestions.contains(cityDetails);
	}

	private void selectIt() {
		for (City city : CityDetailsAutoCompleter.suggestions) {
			if (city.toString().equals(cityDetails)) {
				selectedCity = city;
				relatedInterface.okButton.setDisable(false);
                Interface.appendToStateLn("You selected: " + city.toString());
                break;
			}
			selectedCity = null;
		}
	}

	private void hideAndDeleteOldEntries() {
		for (ContextMenu entries : oldEntries) {
			Platform.runLater(entries::hide);
		}
		oldEntries.clear();
	}
	
	void setText(String newCity) {
		this.value.setText(newCity);
	}
}
