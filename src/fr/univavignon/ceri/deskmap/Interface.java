package fr.univavignon.ceri.deskmap;

import javafx.application.Platform;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.SplitPane;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.stage.Popup;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Insets;
import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.text.Text;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.ScrollBar;
import javafx.stage.Modality;
import javafx.stage.Stage;
import org.xml.sax.SAXException;
import org.controlsfx.glyphfont.FontAwesome;
import org.controlsfx.glyphfont.Glyph;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.List;

/**
 * @author CHAIBAINOU Caroline
 * @author PARRILLA Valentin
 * @author POTIN Clément
 * @author SASSI Nader
 * @version 1.6.1
 */
class Interface {

    private static Stage stage;

    private DataContainer DT;

    /** SplitPane to separate the state bar and the rest */
    private SplitPane upDownPane;

    /** Contains the bottom, the state TextArea */
    private GridPane bottomPane;

    /** We declare a split pane to split the window in two parts : the route research and the map */
    private SplitPane splitPane;

    /** The grid pane that will contain the form and the route area */
    private GridPane searchGrid;

    /** The grid pane will contain our research form, inside searchPane */
    private GridPane formGrid;

    /** Label to set the form's title */
    private Label formTitle;
    /** Instance of the implemented class AutoCompletingTextField, containing the chosen city */
    static AutoCompletingCity cities;

    /** TextField, containing the chosen number in the departure address */
    static TextField numDep;

    /** Instance of the implemented class AutoCompletingTextField, containing the chosen number in the departure address */
    static AutoCompletingStreet roadNameDep;

    /** TextField, containing the chosen number in the departure address */
    static TextField numArr;

    /** Instance of the implemented class AutoCompletingTextField, containing the chosen number in the arrival address */
    static AutoCompletingStreet roadNameArr;

    /**
     * TextArea that will be used to display the route once the route search
     * algorithm will be implemented We want the route block to be outside of the
     * form but inside search pane
     */
    static TextArea route;

    /** Button that will launch the search */
    static Button searchButton;

    /** Button that will stop the search */
    private static Button stopButton;

    /** GridPane containing the map */
    private AnchorPane mapAnchor;

    /**
     * Button to shift towards a direction.
     * Shouldn't be call outside the interface.
     */
    private Button rightShiftButton;

    /** Shouldn't be call outside the interface. */
    private Button leftShiftButton;

    /** Shouldn't be call outside the interface. */
    private Button upperShiftButton;

    /** Shouldn't be call outside the interface. */
    private Button downShiftButton;

    /** represents the box to put the canvas */
    private HBox mapBox;

    /** The button that launches the city's check and allows the user to continue their input */
    Button okButton;

    private Button zoomButton;
    private Button deZoomButton;
    private ScrollBar barZoom;

    /** The canvas that contains the drawn map */
    static Canvas canvasMap;

    /** Contained by the canvas, allow us to draw */
    static GraphicsContext gc;

    /** The main scene, everything has to be added to it in order to be displayed */
    Scene scene;

    /** The non-editable TextArea we'll use to send the user notifications about their actions */
    private static TextArea state;

    private GridPane mapGrid;
    private GridPane allPane;

    // TODO : private Graph routeSearch;

    /** If set to true, will request, parse, ... and draw landuse and railways (aesthetic only). */
    static boolean enableMapBeautification = true;
    private double canvasWidth = 570;
    private double canvasHeight = 570;

	private static CheckBox corresp;

	static CheckBox sense;

	static ComboBox<String> calculationMode;

	static ComboBox<Label> transportMode;
	
	private static Button exportButton;
	
	private Button importButton;

	private static MultiCheckBox busLinesList;

	private static Button addButton;

	private static Button removeButton;

	private static Button openButton;


    /**
     * constructor of Interface, contains all the interface architecture
     */
    Interface(Stage stage) {
        Interface.stage = stage;

        splitPane = new SplitPane();

        upDownPane = new SplitPane();
        upDownPane.setOrientation(Orientation.VERTICAL);
        upDownPane.setPadding(new Insets(5, 5, 5, 5));

        searchGrid = new GridPane();
        searchGrid.setAlignment(Pos.TOP_LEFT);
        searchGrid.setHgap(15);
        searchGrid.setVgap(15);
        searchGrid.setPadding(new Insets(15, 15, 15, 15));

        setForm();
        searchGrid.add(this.formGrid, 0, 0, 2, 1);

        route = new TextArea("Please select a departure and an arrival points to generate an itinerary.");
        openButton = new Button("Maximize\nroute");
        openButton.setDisable(true);
        openButton.setId("openButton");
        openButton.setOnAction(e -> {
        	String route = Interface.route.getText();
            TextArea maxRoute = new TextArea();
            maxRoute.setText(route);
            maxRoute.setEditable(false);
            Scene scene = new Scene(maxRoute);
        	Stage dialog = new Stage();
        	dialog.setTitle("DeskMap - Your route");
        	dialog.setScene(scene);
        	dialog.initOwner(Interface.stage);
        	dialog.initModality(Modality.APPLICATION_MODAL);
        	dialog.showAndWait();
        });
        route.setEditable(false);
        route.setDisable(true);
        searchGrid.add(route, 0, 1);
        searchGrid.add(openButton, 1, 1);

        canvasMap = new Canvas(canvasWidth, canvasHeight);
        gc = canvasMap.getGraphicsContext2D();

        // Add a zoom in boutton
        zoomButton = new Button("+");

        // Add a zoom out boutton
        deZoomButton = new Button("-");

        // Add a ScrollBar for the zoom
        barZoom = new ScrollBar();
        barZoom.setOrientation(Orientation.VERTICAL);
        barZoom.setMin(0);
        barZoom.setMax(16);
        barZoom.setValue(16);
        barZoom.setUnitIncrement(0.1);

        //Button calling their action
        rightShiftButton = new Button("⯈");
        leftShiftButton = new Button("⯇");
        upperShiftButton = new Button("⯅");
        downShiftButton = new Button("⯆");

        // An horizontal Box to contain the map (used to set a background color)
        mapBox = new HBox();
        mapBox.getChildren().add(canvasMap);

        mapBox = new HBox();
        mapBox.getChildren().add(canvasMap);

        mapGrid = new GridPane();
        mapAnchor = new AnchorPane();
        mapAnchor.setMaxSize(canvasWidth, canvasHeight);
        mapAnchor.setMinSize(canvasWidth, canvasHeight);
        mapAnchor.getChildren().add(mapBox);
        setMapButtons();
        mapAnchor.setPadding(new Insets(10, 10, 10, 10));
        mapGrid.add(mapAnchor, 0, 0);

        mapAnchor = new AnchorPane();
        mapAnchor.setPadding(new Insets(25, 25, 25, 25));
        splitPane.getItems().addAll(searchGrid, mapGrid);

        // the pane is split horizontally
        splitPane.setOrientation(Orientation.HORIZONTAL);

        upDownPane.getItems().addAll(splitPane);
        // setHgrow and setVgrow allow us to make upDownPane cells take all the available space in stage
        GridPane.setHgrow(splitPane, Priority.ALWAYS);
        GridPane.setVgrow(splitPane, Priority.ALWAYS);

        // the state block
        String message = "Loading..." + "\n";
        state = new TextArea(message);
        state.setEditable(false);
        state.setPrefRowCount(2);
        bottomPane = new GridPane();
        bottomPane.getChildren().addAll(state);
        upDownPane.getItems().addAll(state);
        GridPane.setHgrow(state, Priority.ALWAYS);
        GridPane.setVgrow(state, Priority.ALWAYS);
        GridPane.setHgrow(upDownPane, Priority.ALWAYS);
        GridPane.setVgrow(upDownPane, Priority.ALWAYS);
        GridPane.setHgrow(mapAnchor, Priority.ALWAYS);
        GridPane.setVgrow(mapAnchor, Priority.ALWAYS);
        GridPane.setHgrow(searchGrid, Priority.ALWAYS);
        GridPane.setVgrow(searchGrid, Priority.ALWAYS);

        allPane = new GridPane();
        allPane.add(upDownPane, 0, 1);

        scene = new Scene(allPane);
        scene.getStylesheets().add(getClass().getResource("Style.css").toExternalForm());
    }

    /**
     * function that sets the style, contains CSS
     */
    void setIdsForCSS() {
        okButton.setId("okButton");
        searchButton.setId("searchButton");
        stopButton.setId("stopButton");
        zoomButton.setId("zoomButton");
        deZoomButton.setId("deZoomButton");
        upperShiftButton.setId("upperShiftButton");
        leftShiftButton.setId("leftShiftButton");
        rightShiftButton.setId("rightShiftButton");
        downShiftButton.setId("downShiftButton");
        barZoom.setId("barZoom");
        exportButton.setId("exportButton");
        importButton.setId("importButton");
        searchGrid.setId("searchGrid");
        mapGrid.setId("mapGrid");
        mapAnchor.setId("mapAnchor");
        mapBox.setId("mapBox");
    }

    /**
     * Function that puts the buttons at the right place in the AnchorPane mapAnchor
     */
    private void setMapButtons() {
        GridPane shiftButtons = new GridPane();
        shiftButtons.add(upperShiftButton, 1, 0);
        shiftButtons.add(leftShiftButton, 0, 1);
        shiftButtons.add(rightShiftButton, 2, 1);
        shiftButtons.add(downShiftButton, 1, 2);

        mapAnchor.getChildren().add(shiftButtons);
        AnchorPane.setTopAnchor(shiftButtons, 15.0);
        AnchorPane.setRightAnchor(shiftButtons, 15.0);

        VBox zoomButtons = new VBox(40.0, zoomButton, deZoomButton);
        HBox zoomControls = new HBox(2.0, zoomButtons, barZoom);
        mapAnchor.getChildren().add(zoomControls);
        AnchorPane.setBottomAnchor(zoomControls, 15.0);
        AnchorPane.setRightAnchor(zoomControls, 15.0);

    }

    private void setForm() {
        formGrid = new GridPane();
        formGrid.setAlignment(Pos.TOP_LEFT);
        formGrid.setHgap(15);
        formGrid.setVgap(15);
        formGrid.setPadding(new Insets(25, 25, 25, 25));
        formGrid.setId("form");

        formTitle = new Label("Find your route now !");
        formTitle.setId("formTitle");
        formTitle.setId("title");
        formGrid.add(formTitle, 0, 0, 3, 1);

        okButton = new Button("OK");
        // Glyph is a class from controlsfx used to set node's graphic with font awesome icons
        Glyph okGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.SEARCH);
        okGlyph.setColor(Color.WHITE);
        okButton.setGraphic(okGlyph);
        formGrid.add(okButton, 4, 1);

        okButton.setOnAction(action -> {

            Runnable backgroundTask = () -> {
                double startTime = System.nanoTime();

                // Reset important vars
                MapHandler.resetMapHandler();

                // Generate the OSM XML files containing the data we need for the city
                try {
                    OSMData.generateAllXMLs(cities.selectedCity);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                // Parse the OSM XML files
                try {
                    DT = Parser.parseOSMCityData(cities.selectedCity);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                if (cities.selectedCity.name.equals("Angers") && cities.selectedCity.country.equals("France")) {
                    appendToState("Getting public transports and stops...");
                    // Get and parse the Angers bus data
                    DT.buses = AngersData.getLiveBus();
                    DT.busStops = AngersData.getBusStop();
                    AngersData.getBusStopTime(DT.busStops);
                    appendToStateLn(" Done.");
                }

                OneWayArrowsGenerator.createArrows(DT);
                CrossroadsFinder.findCrossroads(DT);

                // Convert the lat/lon of each node into x/y pos of the DeskMap's canvas
                Converter.init(DT);
                Converter.recalculateAllXYPos();
                Converter.calculateRoadDistances(DT);

                OneWayArrowsGenerator.calculateAngles(DT);

                // Change the color of the road to gray if the transport mode chosen is not allowed on the road
                for (Road road : (List<Road>)(List<?>) DT.roads) {
                    road.updateRoadColors(Interface.transportMode.getValue().getText());
                }

                BusLinesLink.setLinkAngersOSMDataBusLines();
                // Draw the map
                Drawer.drawMap();

                // Create zoom in/out and drag events, ...
                MapHandler.activateMapEvents(DT);

                double endTime = System.nanoTime();

                Interface.appendToStateLn("→ Done in " + ((endTime - startTime) / 1000000000) + "s.");

                if (cities.selectedCity.name.equals("Angers") && cities.selectedCity.country.equals("France")) {
                    Runnable angersBackgroundTask = () -> {
                        while (cities.selectedCity.name.equals("Angers") && cities.selectedCity.country.equals("France")) {
                            try {
                                Thread.sleep(10_000);
                            } catch (InterruptedException e) {
                                e.printStackTrace();
                            }

                            DT.buses = AngersData.getLiveBus();
                            while (!MapHandler.isReady) {
                                try {
                                    Thread.sleep(1000);
                                } catch (InterruptedException e) {
                                    e.printStackTrace();
                                }
                            }
                            Converter.recalculateDrawablePointsXYPos(DT);

                            Drawer.drawMap();
                        }
                    };

                    Thread angersBackgroundThread = new Thread(angersBackgroundTask);
                    angersBackgroundThread.setDaemon(true);
                    angersBackgroundThread.start();
                    
                }

                // Wait for the map to be drawn to enable the form
                while (!MapHandler.isReady) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                if (DT.transportLines != null) {
                    System.out.println("lines : "+DT.transportLines.size());
                    MultiCheckBox.populateLines(cities.selectedCity.toString(), DT);
                }
                disableFields(false);
            };

            Thread backgroundThread = new Thread(backgroundTask);
            backgroundThread.setDaemon(true);
            backgroundThread.start();
            
        });

        okButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> okButton.setEffect(new DropShadow()));
        // Removing the shadow when the mouse cursor is off
        okButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> okButton.setEffect(null));

        Label dep = new Label("Departure : ");
        formGrid.add(dep, 0, 2);
        numDep = new TextField();
        numDep.setId("numDep");
        formGrid.add(numDep, 1, 2);
        roadNameDep = new AutoCompletingStreet(Road.roadNames);
        roadNameDep.setId("pathDep");
        formGrid.add(roadNameDep, 2, 2, 3, 1);

        numDep.textProperty().addListener((observable, oldValue, newValue) -> checkNumericTextFields(oldValue, newValue, numDep));
        numDep.textProperty().addListener((observable, oldValue, newValue) -> checkTextFields());
        roadNameDep.textProperty().addListener((observable, oldValue, newValue) -> checkTextFields());

        Label arr = new Label("Arrival : ");
        formGrid.add(arr, 0, 3);
        numArr = new TextField();
        numArr.setId("numArr");
        formGrid.add(numArr, 1, 3);
        roadNameArr = new AutoCompletingStreet(Road.roadNames);
        roadNameArr.setId("pathArr");
        formGrid.add(roadNameArr, 2, 3, 3, 1);

        numArr.textProperty().addListener((observable, oldValue, newValue) -> checkNumericTextFields(oldValue, newValue, numArr));
        numArr.textProperty().addListener((observable, oldValue, newValue) -> {checkTextFields();});
        roadNameArr.textProperty().addListener((observable, oldValue, newValue) -> {checkTextFields();});

        Label ped = new Label("Pedestrian");
        Glyph pedGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.MALE);
        pedGlyph.setColor(Color.BLACK);
        ped.setGraphic(pedGlyph);
        ped.setTextFill(Color.BLACK);
        ped.textProperty().addListener((observable, oldValue, newValue) -> {
        	ped.setTextFill(Color.BLACK);
        });

        Label pt = new Label("Public transports");
        Glyph ptGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.BUS);
        ptGlyph.setColor(Color.BLACK);
        pt.setGraphic(ptGlyph);
        pt.setTextFill(Color.BLACK);
        pt.textProperty().addListener((observable, oldValue, newValue) -> {
        	pt.setTextFill(Color.BLACK);
        });

        Label car = new Label("Car");
        Glyph carGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.CAR);
        carGlyph.setColor(Color.BLACK);
        car.setGraphic(carGlyph);
        car.setTextFill(Color.BLACK);
        car.textProperty().addListener((observable, oldValue, newValue) -> {
        	car.setTextFill(Color.BLACK);
        });

        Label bike = new Label("Bike");
        Glyph bikeGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.BICYCLE);
        bikeGlyph.setColor(Color.BLACK);
        bike.setGraphic(bikeGlyph);
        bike.setTextFill(Color.BLACK);
        bike.textProperty().addListener((observable, oldValue, newValue) -> {
        	bike.setTextFill(Color.BLACK);
        });

        ObservableList<Label> transports = FXCollections.observableArrayList(ped, pt, car, bike);
        transportMode = new ComboBox<>(transports);
        transportMode.getSelectionModel().selectFirst();
        transportMode.valueProperty().addListener((observable, oldValue, newValue) -> {
        	ped.setTextFill(Color.BLACK);
        	pt.setTextFill(Color.BLACK);
        	car.setTextFill(Color.BLACK);
        	bike.setTextFill(Color.BLACK);
        });
        transportMode.setOnAction(action -> {
        	ped.setTextFill(Color.BLACK);
        	pt.setTextFill(Color.BLACK);
        	car.setTextFill(Color.BLACK);
        	bike.setTextFill(Color.BLACK);
        });
        formGrid.add(transportMode, 0, 4, 2, 1);

        // Change the color of the road to gray if the transport mode chosen is not allowed on the road
        // Also search for new itinerary depending on the new transport mode chosen
        transportMode.valueProperty().addListener((observableValue, o, t1) -> {
            for (Road road : (List<Road>)(List<?>) DT.roads) {
                road.updateRoadColors(Interface.transportMode.getValue().getText());
            }
            try {
                if (DT.itinerary.departure != null && DT.itinerary.arrival != null) {
                    CrossroadsFinder.selectCrossroad(DT, DT.itinerary.departure, true, false, false);
                    CrossroadsFinder.selectCrossroad(DT, DT.itinerary.arrival, false, false, true);
                }
                else {
                    Drawer.drawMap();
                }
            } catch (IOException | ParserConfigurationException | SAXException e) {
                // e.printStackTrace();
            }
        });
        
        HBox addControls = new HBox();
        addControls.setId("controls");
        ObservableList<String> calculations = FXCollections.observableArrayList("Time", "Distance");
        calculationMode = new ComboBox<>(calculations);
        calculationMode.getSelectionModel().selectFirst();
        addControls.getChildren().add(calculationMode);

        // Recalculate the itinerary if the calculation mode has changed
        calculationMode.valueProperty().addListener((observableValue, o, t1) -> {
            try {
                CrossroadsFinder.selectCrossroad(DT, DT.itinerary.departure, true, false, false);
                CrossroadsFinder.selectCrossroad(DT, DT.itinerary.arrival, false, false, true);
            } catch (IOException | ParserConfigurationException | SAXException e) {
                // e.printStackTrace();
            }
        });
        
        VBox checkboxes = new VBox();

        sense = new CheckBox("Consider directions");
        sense.selectedProperty().addListener((observableValue, o, t1) -> {
            try {
                CrossroadsFinder.selectCrossroad(DT, DT.itinerary.departure, true, false, false);
                CrossroadsFinder.selectCrossroad(DT, DT.itinerary.arrival, false, false, true);
            } catch (IOException | ParserConfigurationException | SAXException | NullPointerException e) {
                // e.printStackTrace();
            }
            Drawer.drawMap();
        });

        corresp = new CheckBox("Include line changes");
        HBox publicTransports = new HBox();
        Button warning = new Button("?");
        warning.setStyle(
                "-fx-background-radius: 5em; " +
                "-fx-min-width: 23px; " +
                "-fx-min-height: 23px; " +
                "-fx-max-width: 23px; " +
                "-fx-max-height: 23px;"
        );
        publicTransports.getChildren().addAll(corresp, warning);
        corresp.setDisable(true);
        checkboxes.getChildren().addAll(sense, publicTransports);
        addControls.getChildren().add(checkboxes);
        formGrid.add(addControls, 2, 4, 2, 1);

        Text warningText = new Text("Warning : this check box is used for public transports route research (not available yet).");
        warningText.setWrappingWidth(200);
        StackPane sp = new StackPane(warningText);
        sp.setPrefSize(200, 50);
        sp.setId("warningStack");
        Popup popup = new Popup();
        popup.setX(Interface.stage.getX()+corresp.getLayoutX());
        popup.setY(Interface.stage.getY()+corresp.getLayoutY());
        popup.getContent().add(sp);
        warning.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> popup.show(Interface.stage));
        warning.addEventHandler(MouseEvent.MOUSE_EXITED, e -> popup.hide());
        
        searchButton = new Button("Search");
        Glyph searchGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.CHECK);
        searchGlyph.setColor(Color.WHITE);
        searchButton.setGraphic(searchGlyph);
        HBox searchStop = new HBox();
        searchStop.setId("searchStop");
        searchStop.getChildren().add(searchButton);
        searchButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> searchButton.setEffect(new DropShadow()));
        // Removing the shadow when the mouse cursor is off
        searchButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> searchButton.setEffect(null));
        searchButton.setDisable(true);
        searchButton.setOnAction(action -> {
            Runnable itinerarySearchTask = () -> {
                searchButton.setDisable(true);
                stopButton.setDisable(false);

                // Departure
                try {
                    Crossroad departure = CrossroadsFinder.findClosestCrossroadFrom(numDep.getText(), roadNameDep.getText(), DT);
                    CrossroadsFinder.selectCrossroad(DT, departure, true, false, false);
                } catch (IOException | ParserConfigurationException | SAXException e) {
                    e.printStackTrace();
                }

                // Arrival
                try {
                    Crossroad arrival = CrossroadsFinder.findClosestCrossroadFrom(numArr.getText(), roadNameArr.getText(), DT);
                    CrossroadsFinder.selectCrossroad(DT, arrival, false, false, true);
                } catch (IOException | ParserConfigurationException | SAXException e) {
                    e.printStackTrace();
                }

                enableRouteButtons();
            };

            Thread itinerarySearchThread = new Thread(itinerarySearchTask);
            itinerarySearchThread.setDaemon(true);
            itinerarySearchThread.start();
        });

        stopButton = new Button("Stop");
        Glyph stopGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.TIMES);
        stopGlyph.setColor(Color.WHITE);
        stopButton.setGraphic(stopGlyph);
        searchStop.getChildren().add(stopButton);
        formGrid.add(searchStop, 0, 5, 3, 1);
        stopButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> stopButton.setEffect(new DropShadow()));
        // Removing the shadow when the mouse cursor is off
        stopButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> stopButton.setEffect(null));
        stopButton.setDisable(true);
        stopButton.setOnAction(action -> {
            searchButton.setDisable(false);
            stopButton.setDisable(true);
        });

        Label busLines = new Label("Select the bus lines you want to display in the list bellow :");
        formGrid.add(busLines, 0, 6, 4, 1);

        // hard coded for now but will soon be added thanks to request to Angers' API
        busLinesList = new MultiCheckBox(this);
        busLinesList.setDisable(true);
        busLinesList.setMaxWidth(250);
        formGrid.add(busLinesList, 0, 7, 3, 1);

        addButton = new Button("Add");
        addButton.setId("addButton");
        addButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> addButton.setEffect(new DropShadow()));
        addButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> addButton.setEffect(null));
        addButton.setOnAction(actionEvent -> {
        	for(TransportLine tl : MultiCheckBox.toDraw) {
        		tl.visible = true;
        		for (Drawable transportLine : DT.transportLines){
                	TransportLine tl2 = (TransportLine) transportLine;
                    if(tl.name.equals(tl2.name)) tl2.visible = true;
                }
        		MultiCheckBox.reset();
        	}  
        	Drawer.drawMap();
        });
        
        removeButton = new Button("Remove");
        removeButton.setId("removeButton");
        removeButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> removeButton.setEffect(new DropShadow()));
        removeButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> removeButton.setEffect(null));
        removeButton.setOnAction(actionEvent -> {
        	for(TransportLine tl : MultiCheckBox.toDraw) {
        		tl.visible = false;
        		for (Drawable transportLine : DT.transportLines){
                    if(tl.name.equals(transportLine.name)) ((TransportLine) transportLine).visible = false;
                }
        		MultiCheckBox.reset();
        	}
        	Drawer.drawMap();
        });
        
        
        HBox addRemove = new HBox();
        addRemove.getChildren().addAll(addButton, removeButton);
        addRemove.setId("addRemove");
        formGrid.add(addRemove, 3, 7);

        disableFields(true);

        cities = new AutoCompletingCity("city", this);
        formGrid.add(cities, 0, 1, 3, 1);
        
        cities.value.textProperty().addListener((observable, oldValue, newValue) -> {

        	emptyFields();
        	disableFields(true);

            // Request suggestions for the current typed name to the Photon API
            cities.refreshList(newValue);

            // checkCity();
            // if(!okButton.isDisabled()) changedCity();
        });

        
        HBox exportImport = new HBox();
        exportButton = new Button("Export");
        Glyph exportGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.DOWNLOAD);
        exportGlyph.setColor(Color.WHITE);
        exportButton.setGraphic(exportGlyph);
        exportButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> exportButton.setEffect(new DropShadow()));
        exportButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> exportButton.setEffect(null));
        exportButton.setDisable(true);
        exportImport.getChildren().add(exportButton);
        importButton = new Button("Import");
        Glyph importGlyph = new Glyph("FontAwesome", FontAwesome.Glyph.UPLOAD);
        importGlyph.setColor(Color.WHITE);
        importButton.setGraphic(importGlyph);
        importButton.addEventHandler(MouseEvent.MOUSE_ENTERED, e -> importButton.setEffect(new DropShadow()));
        importButton.addEventHandler(MouseEvent.MOUSE_EXITED, e -> importButton.setEffect(null));
        exportImport.getChildren().add(importButton);
        exportImport.setSpacing(50);
        searchGrid.add(exportImport, 0, 2);

        exportButton.setOnAction(actionEvent -> {
            FileManager.saveItinerary(cities.selectedCity);
        });
        
        importButton.setOnAction(actionEvent -> {
        	try {
        		System.out.println("Import file...");
				XmlManager.importItinerary(FileManager.openItinerary());
				XmlManager.form.updateAfterImport();
			} catch (ClassNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (SAXException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

        });
        
    }


    /**
     * function that checks whether all fields are set or not and enables
     * searchButton or not as a consequence
     */
    private void checkTextFields() {
    	boolean validStreets = roadNameDep.isValid() & roadNameArr.isValid();
        String pathD = roadNameDep.getText();
        String pathA = roadNameArr.getText();

        if (pathD.isEmpty() || pathA.isEmpty() || !validStreets) {
            searchButton.setDisable(true);
        }
        else {
            searchButton.setDisable(false);
        }
    }


    /**
     * function called by the numeric TextFields to block user from entering
     * non-numeric values
     *
     * @param oldV : will be set as the value of the field before we changed it
     * @param newV : will be set as the value of the field while we are trying to
     *             set it differently
     * @param num  : the field we are changing
     */
    private void checkNumericTextFields(String oldV, String newV, TextField num) {
        searchButton.setDisable(true);
        stopButton.setDisable(true);
        if (!newV.matches("\\d{0,4}?")) {
            state.appendText("WARNING : that field can contain only numeric values.\n");
            num.setText(oldV);
        }
    }

    static void disableFields(boolean state) {
        numDep.setDisable(state);
        numArr.setDisable(state);
        roadNameDep.setDisable(state);
        roadNameArr.setDisable(state);
        corresp.setDisable(state);
        sense.setDisable(state);
        calculationMode.setDisable(state);
        transportMode.setDisable(state);
        addButton.setDisable(state);
        removeButton.setDisable(state);
        busLinesList.setDisable(state);
    }

    /**
     * Adds texts to the state bar.
     *
     * @param message String we want to append to the state block.
     */
    static void appendToState(String message) {
        Platform.runLater(() -> {
            state.appendText(message);
            System.out.print(message);
        });
    }

    /**
     * Adds texts to the state bar with a line break at the end.
     *
     * @param message String we want to append to the state block.
     * @since 1.4.1
     */
    static void appendToStateLn(String message) {
        Platform.runLater(() -> {
            state.appendText(message + "\n");
            System.out.println(message);
        });
    }

    static void resetRoute() {
        route.clear();
    }

    static void appendToRoute(String message) {
        route.appendText(message);
    }

    static void appendToRouteLn(String message) {
        route.appendText(message + "\n");
    }

    static void appendStepToRoute(int index, String roadName, double distance, double time) {
        if (roadName.isEmpty()) {
            roadName = "<Missing road name>";
        }
        int roundedDistance = (int) Math.round(distance);
        int roundedTime = (int) Math.round(time);

        Interface.appendToRouteLn(index + " Follow \"" + roadName + "\" for " + roundedDistance + "m (" + roundedTime + "s)");
    }

    static void emptyFields() {
    	numDep.setText("");
    	numArr.setText("");
    	roadNameDep.setText("");
    	roadNameArr.setText("");
    }

    static void enableRouteButtons() {
    	stopButton.setDisable(true);
        route.setDisable(false);
        openButton.setDisable(false);
        exportButton.setDisable(false);
    }
}
