package fr.univavignon.ceri.deskmap;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.text.Normalizer;
import java.util.List;

class CrossroadsFinder {

    private static String NOMINATIM_REVERSE_URL = "https://nominatim.openstreetmap.org/reverse?";


	static void findCrossroads(DataContainer DT) {
        double startTime = System.nanoTime();
        Interface.appendToState("Generating crossroads...");

		for (Road road : (List<Road>)(List<?>) DT.roads) {
		    // Get the first and last node of the road
            Node firstNode = road.nodes.get(0);
            Node lastNode = road.nodes.get(road.nodes.size() - 1);

            // Create crossroad objects
			Crossroad first = new Crossroad(firstNode, road.name);
            Crossroad last = new Crossroad(lastNode, road.name);

            // Add the 'first' crossroad if no crossroad of the same coordinates is already in the list
            checkIfCrossroadAtSameCoordinates(DT, first, last, road);

            // Do the same for the 'last' crossroad
            checkIfCrossroadAtSameCoordinates(DT, last, first, road);
		}

        double endTime = System.nanoTime();
        int elapsedTime = (int)((endTime - startTime)/ 1000000000);

        Interface.appendToStateLn("\tParsed roads. Done in " + elapsedTime + "s");
	}

	private static void checkIfCrossroadAtSameCoordinates(DataContainer DT, Crossroad current, Crossroad otherEnd, Road road) {
	    // 'first' will be either an already existing crossroad that have the same coordinates than 'current',
        // either 'current' if none was found
        Crossroad first = getCrossroadOfTheSameCoords(DT, current);

        // Same for 'last' with 'otherEnd'
        Crossroad last = getCrossroadOfTheSameCoords(DT, otherEnd);

        // Add the neighbour if the 'first' crossroad is not the same than the 'last' crossroad (weird but that fixes a bug..)
        if (first != last) {
            first.neighbours.put(last, road);
            first.addRoadName(road.name);
        }

        // If not already in the list
        if (current == first) {
            DT.crossroads.add(current);
        }
    }

    private static Crossroad getCrossroadOfTheSameCoords(DataContainer DT, Crossroad crossroad) {
	    for (Crossroad existingCrossroad : DT.crossroads) {
            if (crossroad.node.lon == existingCrossroad.node.lon && crossroad.node.lat == existingCrossroad.node.lat) {
                return existingCrossroad;
            }
        }
	    return crossroad;
    }

    static void selectCrossroad(DataContainer DT, Crossroad selectedCrossroad, boolean isDeparture, boolean actualizeForm, boolean searchForItinerary) throws IOException, ParserConfigurationException, SAXException {
        Itinerary itinerary = DT.itinerary;
        Crossroad selected;
        Crossroad itineraryOtherEnd;

        if (isDeparture) {
            selected = itinerary.departure;
            itineraryOtherEnd = itinerary.arrival;
        }
        else {
            selected = itinerary.arrival;
            itineraryOtherEnd = itinerary.departure;
        }

        // Reset the old selected node
        if (selected != null) {
            selected.resetDrawingParameters();
        }

        // Check if the departure node is the arrival node, if so, delete the old departure or arrival node
        if (itineraryOtherEnd != null) {
            if (selectedCrossroad == itineraryOtherEnd) {
                if (isDeparture) {
                    itinerary.arrival = null;
                }
                else {
                    itinerary.departure = null;
                }
            }
        }

        // Actualize the new selected node
        selected = selectedCrossroad;
        if (isDeparture) {
            itinerary.departure = selected;
        }
        else {
            itinerary.arrival = selected;
        }
        Converter.itinerary = itinerary;

        // Actualize the form if needed
        if (actualizeForm) {
            fillInForm(isDeparture, selected);
        }

        // Actualize the selected node's drawing parameters
        selected.applySelectedDrawingParameters(isDeparture);

        // Reset old itinerary
        DT.itinerary.nodes.clear();
        Interface.resetRoute();

        // Check if there is a path between the departure and arrival nodes
        if (searchForItinerary) {
            if (itinerary.departure != null && itinerary.arrival != null) {
                List<Road> path = new AStar(DT.crossroads, itinerary.departure, itinerary.arrival).findPath();
                DT.itinerary.makeNewItineraryFromPath(path);
                Interface.enableRouteButtons();
                Interface.searchButton.setDisable(true);
            }
            else {
                Interface.appendToRouteLn("Please select a departure and an arrival points to generate an itinerary.");
            }
        }
        else {
            Interface.appendToRouteLn("Please select a departure and an arrival points to generate an itinerary.");
        }

        // Redraw the map
        Drawer.drawMap();
    }

    private static void fillInForm(boolean isDeparture, Crossroad crossroad) throws IOException, ParserConfigurationException, SAXException {
        String houseNumber;
        String roadName;

        if (isDeparture) {
            houseNumber = Interface.numDep.getText();
            roadName = Interface.roadNameDep.getText();
        }
        else {
            houseNumber = Interface.numArr.getText();
            roadName = Interface.roadNameArr.getText();
        }

        // Prepare the query
        String query = "format=xml&lat=" + crossroad.node.lat + "&lon=" + crossroad.node.lon + "&zoom=18&addressdetails=1";

        // Send a request to the Nominatim API to find the closest address from the chosen crossroad
        String result = Requester.sendRequest(NOMINATIM_REVERSE_URL, query, false);

        // Parse the results
        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(result)));

        NodeList entries = doc.getElementsByTagName("*");
        int entriesLength = entries.getLength();

        for (int i = 0; i < entriesLength; i++) {
            Element element = (Element) entries.item(i);

            switch (element.getNodeName()) {
                case "house_number":
                    houseNumber = element.getTextContent();
                    break;
                case "road":
                    roadName = element.getTextContent();
                    break;
            }
        }

        // Set the new values
        if (isDeparture) {
            Interface.numDep.setText(houseNumber);
            Interface.roadNameDep.setText(roadName);
        }
        else {
            Interface.numArr.setText(houseNumber);
            Interface.roadNameArr.setText(roadName);
        }
    }

    static Crossroad findClosestCrossroadFrom(String houseNumber, String address, DataContainer DT) throws IOException, ParserConfigurationException, SAXException {
	    String url = "https://nominatim.openstreetmap.org/search?format=xml&q=";
	    String query = houseNumber + "+" + address + "+" + DT.city.name + "+" + DT.city.country;

	    // Replace spaces and dashes to "+"
	    query = query.replace(" ", "+").replace("-", "+");

	    // Remove accents
        query = Normalizer.normalize(query, Normalizer.Form.NFD);
        query = query.replaceAll("[\\p{InCombiningDiacriticalMarks}]", "");

        // Send a request to the Nominatim API to find the closest address from the chosen crossroad
        String result = Requester.sendRequest(url, query, false);

        // Parse the results
        double lat = DT.city.lat;
        double lon = DT.city.lon;

        DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
        dbf.setValidating(false);

        DocumentBuilder db = dbf.newDocumentBuilder();
        Document doc = db.parse(new InputSource(new StringReader(result)));

        NodeList entries = doc.getElementsByTagName("*");
        int entriesLength = entries.getLength();

        for (int i = 0; i < entriesLength; i++) {
            Element element = (Element) entries.item(i);

            switch (element.getNodeName()) {
                case "place":
                    lat = Double.parseDouble(element.getAttribute("lat"));
                    lon = Double.parseDouble(element.getAttribute("lon"));
                    break;
            }
        }

        // Find the closest crossroad to the address lat/lon
        Crossroad closest = DT.crossroads.get(0);
        double minDistance = DistanceCalculator.getDistanceInMeters(lat, lon, closest.node.lat, closest.node.lon);

        for (Crossroad crossroad : DT.crossroads) {
            double currentDistance = DistanceCalculator.getDistanceInMeters(lat, lon, crossroad.node.lat, crossroad.node.lon);
            if (currentDistance < minDistance) {
                closest = crossroad;
                minDistance = currentDistance;
            }
        }

        return closest;
    }
}
