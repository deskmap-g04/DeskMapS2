package fr.univavignon.ceri.deskmap;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javafx.scene.control.TextArea;

class XmlManager {
	
	static ImportedForm form;
	static ImportedItinerary itinerary;

	//public static String classNode = "fr.univavignon.ceri.deskmap.Node";
	static void importItinerary(File fileToImport) throws SAXException, IOException, ClassNotFoundException {
		File file = fileToImport;
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder;
		try {			
	        dbFactory.setValidating(false);
	        DocumentBuilder db = dbFactory.newDocumentBuilder();     
	        Document doc = db.parse(fileToImport);
	        NodeList entries = doc.getElementsByTagName("*");
	        int entriesLength = entries.getLength();
	        List<DrawableIcon> busList = new ArrayList<>();
	        List<String> informations = new ArrayList<>();
	        List<fr.univavignon.ceri.deskmap.Node> nodes = new ArrayList<>();
	        NodeList listNodes = doc.getElementsByTagName("nodes");
	        NodeList listInfo = doc.getElementsByTagName("information");
	        NodeList listBus = doc.getElementsByTagName("buses");
	        
	        String cityName = "";
	        String numDeparture = "";
	        String numArrival = "";
	        String departureStreetName = "";
	        String arrivalStreetName = "";
	        City city = null;
	        Crossroad departure = null;
	        Crossroad arrival = null;
	        
	        // Parse the XML file
	        for (int i = 0; i < entriesLength; i++) {
	            Element element = (Element) entries.item(i);

	            // New way/relation encountered
	            switch (element.getNodeName()) {
	            
	            	case "city":  
	            		String country = element.getAttribute("country");
	            		cityName = element.getAttribute("name");
	            		String state = element.getAttribute("state");
	            		Double lon = Double.parseDouble(element.getAttribute("lon"));
	            		Double lat = Double.parseDouble(element.getAttribute("lat"));
	            		city = new City(0, cityName, country, state, lon, lat);          
	            		break;
	            		
	            	case "departure":
	            		fr.univavignon.ceri.deskmap.Node depart = new fr.univavignon.ceri.deskmap.Node(Double.parseDouble(element.getAttribute("lon")), Double.parseDouble(element.getAttribute("lat")));
	            		departure = new Crossroad(depart, "departure");
	            		numDeparture = element.getAttribute("num");
	            		departureStreetName = element.getAttribute("roadName");
	            		break;
	            		
	            	case "arrival":
	            		fr.univavignon.ceri.deskmap.Node arrived = new fr.univavignon.ceri.deskmap.Node(Double.parseDouble(element.getAttribute("lon")), Double.parseDouble(element.getAttribute("lat")));
	            		arrival = new Crossroad(arrived, "arrival");
	            		numArrival = element.getAttribute("num");
	            		arrivalStreetName = element.getAttribute("roadName");
	            		break;
	            		
	            	case "node":
	            		fr.univavignon.ceri.deskmap.Node node = new fr.univavignon.ceri.deskmap.Node(Long.parseLong(element.getAttribute("id")),Double.parseDouble(element.getAttribute("lon")), Double.parseDouble(element.getAttribute("lat")));
	            		nodes.add(node);
	            		break;
	            		
	            	case "info":
	            		informations.add(element.getAttribute("text"));
	            		break;
	            		
	            	case "bus":
	            		fr.univavignon.ceri.deskmap.Node bus = new fr.univavignon.ceri.deskmap.Node(Long.parseLong(element.getAttribute("id")),Double.parseDouble(element.getAttribute("lon")), Double.parseDouble(element.getAttribute("lat")));
	            		String busName = "bus number "+element.getAttribute("id");
	            		DrawableIcon iconBus = new DrawableIcon(busName, bus);
	            		busList.add(iconBus);
	            		break;
	            }
	        }
	        
	        
	        ImportedItinerary itinerary = new ImportedItinerary(departure, arrival, nodes);
	        
	        ImportedForm form = new ImportedForm(city, numDeparture, departureStreetName, numArrival, arrivalStreetName, informations);
	        XmlManager.form = form;
	        form.updateAfterImport();
	        System.out.println("Itineraire : ");
	        itinerary.toString();
	        System.out.println("--------------------");
	        System.out.println("Informations du formulaire :");
	        form.toString();
	        System.out.println("--------------------");
	        System.out.println("Position des bus :");
	        for(DrawableIcon b : busList) {
	        	System.out.println(b.name + " lon : " + b.node.lon + " lat : " + b.node.lat);
	        }
	        

      
		} catch (ParserConfigurationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
        
	}
	
    static void export(City localCity, File fileToExport) {

        Itinerary itinerary = Converter.itinerary;

        try {

            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();

            // itinary element
            Element root = document.createElement("itinerary");
            document.appendChild(root);

            // city element
            Element city = document.createElement("city");
            root.appendChild(city);

            Attr attr = document.createAttribute("name");
            attr.setValue(localCity.name);
            city.setAttributeNode(attr);

            attr = document.createAttribute("country");
            attr.setValue(localCity.country);
            city.setAttributeNode(attr);

            attr = document.createAttribute("state");
            attr.setValue(localCity.state);
            city.setAttributeNode(attr);

            double lon = localCity.lon;
            double lat = localCity.lat;

            attr = document.createAttribute("lon");
            attr.setValue(Double.toString(lon));
            city.setAttributeNode(attr);

            attr = document.createAttribute("lat");
            attr.setValue(Double.toString(lat));
            city.setAttributeNode(attr);




            // departure element
            Element departure = document.createElement("departure");
            root.appendChild(departure);

            lon = itinerary.departure.node.lon;
            lat = itinerary.departure.node.lat;

            attr = document.createAttribute("lon");
            attr.setValue(Double.toString(lon));
            departure.setAttributeNode(attr);


            attr = document.createAttribute("lat");
            attr.setValue(Double.toString(lat));
            departure.setAttributeNode(attr);

            attr = document.createAttribute("num");
            attr.setValue(getNumDeparture());
            departure.setAttributeNode(attr);

            attr = document.createAttribute("roadName");
            attr.setValue(getRoadDeparture());
            departure.setAttributeNode(attr);

            // arrival element
            Element arrival = document.createElement("arrival");
            root.appendChild(arrival);

            lon = itinerary.arrival.node.lon;
            lat = itinerary.arrival.node.lat;

            attr = document.createAttribute("lon");
            attr.setValue(Double.toString(lon));
            arrival.setAttributeNode(attr);


            attr = document.createAttribute("lat");
            attr.setValue(Double.toString(lat));
            arrival.setAttributeNode(attr);

            attr = document.createAttribute("num");
            attr.setValue(getNumArrival());
            arrival.setAttributeNode(attr);

            attr = document.createAttribute("roadName");
            attr.setValue(getRoadArrival());
            arrival.setAttributeNode(attr);


            // nodes element
            Element nodes = document.createElement("nodes");
            root.appendChild(nodes);

            int id = 1;

            for (fr.univavignon.ceri.deskmap.Node ItineraryNode : getItineraryNodes(itinerary)) {
                if (ItineraryNode != null){
                    // node element
                    Element node = document.createElement("node");
                    nodes.appendChild(node);

                    attr = document.createAttribute("id");
                    attr.setValue(Integer.toString(id));
                    node.setAttributeNode(attr);

                    lon = ItineraryNode.lon;
                    lat = ItineraryNode.lat;

                    attr = document.createAttribute("lon");
                    attr.setValue(Double.toString(lon));
                    node.setAttributeNode(attr);


                    attr = document.createAttribute("lat");
                    attr.setValue(Double.toString(lat));
                    node.setAttributeNode(attr);

                    id += 1;
                }
            }

            // information element
            Element information = document.createElement("information");
            root.appendChild(information);
            String[] infos = getItineraryInformation();
            id = 1;
            for (String line : infos){
                // node element
                Element node = document.createElement("info");
                //node.appendChild(document.createTextNode("James"));
                information.appendChild(node);

                attr = document.createAttribute("id");
                attr.setValue(Integer.toString(id));
                node.setAttributeNode(attr);

                attr = document.createAttribute("text");
                attr.setValue(line);
                node.setAttributeNode(attr);

                id += 1;
            }

            City currentCity = Interface.cities.selectedCity;
            if (currentCity.name.equals("Angers") && currentCity.country.equals("France")) {
                List<DrawableIcon> buses = Converter.refreshableMapPoints.get(0);
                Element busesPosition = document.createElement("buses");
                root.appendChild(busesPosition);
                id = 1;
                for (DrawableIcon bus : buses){
                    // node element
                    Element node = document.createElement("bus");
                    //node.appendChild(document.createTextNode("James"));
                    busesPosition.appendChild(node);

                    attr = document.createAttribute("id");
                    attr.setValue(Integer.toString(id));
                    node.setAttributeNode(attr);

                    lon = bus.node.lon;
                    lat = bus.node.lat;

                    attr = document.createAttribute("lon");
                    attr.setValue(Double.toString(lon));
                    node.setAttributeNode(attr);


                    attr = document.createAttribute("lat");
                    attr.setValue(Double.toString(lat));
                    node.setAttributeNode(attr);

                    id += 1;
                }
            }

            // create the xml file
            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            DOMSource domSource = new DOMSource(document);
            StreamResult streamResult = new StreamResult(fileToExport);

            transformer.transform(domSource, streamResult);

        } catch (ParserConfigurationException | TransformerException pce) {
            pce.printStackTrace();
        }
    }

    private static List<fr.univavignon.ceri.deskmap.Node> getItineraryNodes(Itinerary itinerary){
        List<fr.univavignon.ceri.deskmap.Node> itineraryNodes = itinerary.nodes;
        return itineraryNodes;
    }


    private static String[] getItineraryInformation(){
        TextArea informations = Interface.route;
        return informations.getText().split("\\n");
    }

    private static String getRoadDeparture(){
        return Interface.roadNameDep.getText();
    }

    private static String getRoadArrival(){
        return Interface.roadNameArr.getText();
    }

    private static String getNumDeparture(){
        return Interface.numDep.getText();
    }

    private static String getNumArrival(){
        return Interface.numArr.getText();
    }
}
