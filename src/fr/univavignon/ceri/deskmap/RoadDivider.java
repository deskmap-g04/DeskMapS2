package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.List;

public class RoadDivider implements Runnable {

    static int counter = 0;

    private List<Drawable> roads;
    private List<Drawable> dividedRoads;

    private int poolSize;
    private int indexInPool;


    RoadDivider(List<Drawable> roads, List<Drawable> dividedRoads, int poolSize, int indexInPool) {
        this.roads = roads;
        this.dividedRoads = dividedRoads;

        this.poolSize = poolSize;
        this.indexInPool = indexInPool;
    }

    public void run() {
        double sectionFraction = 1f / poolSize;
        int sectionSize = (int)(roads.size() * sectionFraction);

        int sectionStartIndex = indexInPool * sectionSize;
        int sectionEndIndex;
        if (indexInPool == poolSize - 1)
            sectionEndIndex = roads.size();
        else
            sectionEndIndex = (indexInPool + 1) * sectionSize;

        for (Drawable road : roads.subList(sectionStartIndex, sectionEndIndex)) {
            dividedRoads.addAll(subdivideRoad((Road) road, roads));
        }
    }

    private static List<Drawable> subdivideRoad(Road road, List<Drawable> roads) {
        List<Drawable> dividedRoad = new ArrayList<>();

        Node roadStart = road.nodes.get(0);
        Node roadEnd = road.nodes.get(road.nodes.size() - 1);

        int lastNodeIndex = 0;
        int index = 0;

        // Find all the roads that have similar bounds
        List<Road> potentialIntersections = new ArrayList<>();
        for (Road otherRoad : (List<Road>) (List<?>) roads) {
            if (road.overlaps(otherRoad)) {
                potentialIntersections.add(otherRoad);
            }
        }

        // Check if there is intersection between the current road and any of the roads found above
        for (Node node : road.nodes) {
            if (node.id != roadStart.id && node.id != roadEnd.id) {
                for (Road otherRoad : potentialIntersections) {
                    if (road.id != otherRoad.id) {
                        for (Node otherNode : otherRoad.nodes) {
                            if (node.equals(otherNode)) {
                                counter++;
                                Drawable newRoad = Parser.createSpecificRoad(road.id, road.nodes.subList(lastNodeIndex, index + 1), road.name, road.type, road.layer, road.speedLimit, road.isOneWay, new double[]{road.minLat, road.minLon, road.maxLat, road.maxLon});
                                dividedRoad.add(newRoad);
                                lastNodeIndex = index;
                            }
                        }
                    }
                }
            }
            index++;
        }

        if (lastNodeIndex == 0 && index == road.nodes.size()) {
            dividedRoad.add(road);
        }
        else {
            Road newRoad = Parser.createSpecificRoad(road.id, road.nodes.subList(lastNodeIndex, road.nodes.size()), road.name, road.type, road.layer, road.speedLimit, road.isOneWay, new double[]{road.minLat, road.minLon, road.maxLat, road.maxLon});
            dividedRoad.add(newRoad);
        }

        return dividedRoad;
    }
}