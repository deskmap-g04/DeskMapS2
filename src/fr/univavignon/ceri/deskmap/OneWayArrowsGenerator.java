package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.List;

class OneWayArrowsGenerator {

    static void createArrows(DataContainer DT) {
        List<DrawableIcon> oneWayArrows = new ArrayList<>();

        for (Road road : (List<Road>)(List<?>) DT.roads) {
            if (road.isOneWay && road.nodes.size() > 1) {
                int middleNodeIndex = road.nodes.size() / 2;
                Node middleNode = road.nodes.get(middleNodeIndex);
                Node previous;
                Node next;

                if (road.nodes.size() == 2) {
                    previous = road.nodes.get(0);
                    next = road.nodes.get(1);
                }
                else {
                    previous = road.nodes.get(middleNodeIndex - 1);
                    next = road.nodes.get(middleNodeIndex + 1);
                }

                oneWayArrows.add(new OneWayArrow("", middleNode, previous, next));
            }
        }

        DT.oneWayArrows = oneWayArrows;
    }

    static void calculateAngles(DataContainer DT) {
        for (OneWayArrow oneWayArrow : (List<OneWayArrow>)(List<?>) DT.oneWayArrows) {
            // Calculate the angle of the arrow
            int angle;
            angle = getAngle(oneWayArrow.previous, oneWayArrow.next);

            oneWayArrow.angle = angle;
        }
    }

    private static int getAngle(Node A, Node B) {
        int angle = (int) Math.toDegrees(Math.atan2(B.yPos - A.yPos, B.xPos - A.xPos)) + 90;

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }
}
