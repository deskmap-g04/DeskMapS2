package fr.univavignon.ceri.deskmap;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.input.MouseEvent;

import java.util.HashMap;
import java.util.Map;


public class BusStopsMenu {

    static Map<String, String> clickedItems;

    /**
     * function to add and display the pop-up event on a busStop
     * @param busStop
     * @param mouseEvent
     * @param DT
     * @return
     */
    static ContextMenu display(BusStop busStop, MouseEvent mouseEvent, DataContainer DT) {
        double screenX = mouseEvent.getScreenX();
        double screenY = mouseEvent.getScreenY();

        // Create ContextMenu
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setAutoHide(true);


        // Create the ContextMenu content
        MenuItem nameBusStop = new MenuItem("Bus Stop : " + busStop.name);

        // Disable the items we want to show as simple labels (not clickable)
        nameBusStop.setDisable(true);

        // Make them look like labels instead of clickable items
        nameBusStop.getStyleClass().add("context-menu-label");


        // Add MenuItems to the ContextMenu and show it at the click's coordinates
        contextMenu.getItems().addAll(nameBusStop,new SeparatorMenuItem());
        getClickedItems(DT);
        for (String bs : busStop.busLineList){
            String bsOSM = BusLinesLink.linkAngersOSMDataBusLines.get(bs);
            if (bsOSM != null){
                MenuItem busItem = setItemText(bsOSM, bs, busStop);
                busItem.setOnAction(e -> {
                    String hexaColor = "yellow";
                    for (Drawable transportLine : DT.transportLines){
                        if (((TransportLine) transportLine).ref.equals(bsOSM)){
                            boolean visible = ((TransportLine) transportLine).visible;
                            hexaColor = ((TransportLine) transportLine).hexaColorCode;
                            if (!visible){
                                ((TransportLine) transportLine).visible = true;
                            }else{
                                ((TransportLine) transportLine).visible = false;
                            }
                        }
                    }
                    if(isClicked(bsOSM)){
                        clickedItems.remove(bsOSM);
                    }else{
                        clickedItems.put(bsOSM,hexaColor);
                    }
                    Drawer.drawMap();
                });
                contextMenu.getItems().add(busItem);
            }else{
                MenuItem busItem = new MenuItem("BusLine : "+ bs);
                busItem.getStyleClass().add("context-menu-label");
                busItem.setDisable(true);
                contextMenu.getItems().add(busItem);
            }
        }

        contextMenu.show(Interface.canvasMap, screenX, screenY);

        return contextMenu;
    }

    /**
     * fucntion to set the item text with his passage time
     * @param bsLineOSM
     * @param busLineAngers
     * @param busStop
     * @return
     */
    static MenuItem setItemText(String bsLineOSM, String busLineAngers, BusStop busStop){
        String busTime = busStop.busesTime.get(busLineAngers);
        MenuItem menuItem;
        if(busTime != null)
        {
            menuItem = new MenuItem("BusLine : "+ bsLineOSM + " | Next passage time : " + getTime(busTime));
        }
        else
        {
            menuItem = new MenuItem("BusLine : "+ bsLineOSM);
        }
        if (isClicked(bsLineOSM))
        {
            menuItem.setStyle("-fx-background-color:" + clickedItems.get(bsLineOSM) + ";");
        }
        return menuItem;
    }

    /**
     * function to split the passage time and get the hours second and minutes
     * @param busTime
     * @return
     */
    static String getTime(String busTime)
    {
        String[] arrOfStr = busTime.split("[T F +]+");
        for(String a : arrOfStr)
        {
            busTime = arrOfStr[1];
        }
        return busTime;
    }

    /**
     * fonction to check if a busLine is clicked
     * @param bsLine
     * @return
     */
    static boolean isClicked(String bsLine){
        if (clickedItems.get(bsLine) == null){
            return false;
        }else{
            return true;
        }
    }

    /**
     * fonction to build the list of the drawn busLines
     * @param DT
     */
    static void getClickedItems(DataContainer DT){
        clickedItems = new HashMap<>();
        for (Drawable transportLine : DT.transportLines){
            boolean visible = ((TransportLine) transportLine).visible;
            String bsLine = ((TransportLine) transportLine).ref;
            if (clickedItems.get(bsLine) == null){
                if (visible){
                    String hexaColor = ((TransportLine) transportLine).hexaColorCode;
                    clickedItems.put(bsLine,hexaColor);
                }
            }
        }
    }


}

