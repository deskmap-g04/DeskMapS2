package fr.univavignon.ceri.deskmap;

import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.SeparatorMenuItem;
import javafx.scene.input.MouseEvent;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.util.Map;

class CrossroadMenu {

    static ContextMenu display(Crossroad crossroad, MouseEvent mouseEvent, DataContainer DT) {
        double screenX = mouseEvent.getScreenX();
        double screenY = mouseEvent.getScreenY();

        // Create ContextMenu
        ContextMenu contextMenu = new ContextMenu();
        contextMenu.setAutoHide(true);

        String roadNamesContent = "Roads connected with this node:";
        for (String name : crossroad.streetNames) {
            roadNamesContent += "\n- " + name;
        }

        // Create the ContextMenu content
        MenuItem latItem = new MenuItem("Latitude: " + crossroad.node.lat);
        MenuItem lonItem = new MenuItem("Longitude: " + crossroad.node.lon);
        MenuItem roadNamesItem = new MenuItem(roadNamesContent);
        MenuItem setAsDepartureItem = new MenuItem("Set as departure");
        MenuItem setAsArrivalItem = new MenuItem("Set as arrival");

        // Disable the items we want to show as simple labels (not clickable)
        latItem.setDisable(true);
        lonItem.setDisable(true);
        roadNamesItem.setDisable(true);

        // Make them look like labels instead of clickable items
        latItem.getStyleClass().add("context-menu-label");
        lonItem.getStyleClass().add("context-menu-label");
        roadNamesItem.getStyleClass().add("context-menu-label");

        // Add actions to the clickable items
        setAsDepartureItem.setOnAction(e -> {
            try {
                CrossroadsFinder.selectCrossroad(DT, crossroad, true, true, true);
            } catch (IOException | ParserConfigurationException | SAXException ex) {
                ex.printStackTrace();
            }
        });
        setAsArrivalItem.setOnAction(e -> {
            try {
                CrossroadsFinder.selectCrossroad(DT, crossroad, false, true, true);
            } catch (IOException | ParserConfigurationException | SAXException ex) {
                ex.printStackTrace();
            }
        });

        // Add MenuItems to the ContextMenu and show it at the click's coordinates
        contextMenu.getItems().addAll(latItem, lonItem, roadNamesItem, new SeparatorMenuItem(), setAsDepartureItem, setAsArrivalItem);
        contextMenu.show(Interface.canvasMap, screenX, screenY);

        return contextMenu;
    }
}
