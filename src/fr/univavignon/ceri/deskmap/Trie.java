package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.List;

/**
 * @author LA Vivien 
 * @source : https://www.youtube.com/watch?v=wjWSfRfiiIs
 * A Trie is a data structure similar to a Tree which makes browsing words with a prefix easier. 
 * Each node contains a letter that can be linked to another node.
 * A child node that is linked to several parent nodes but doesn't have any child is considered as a final letter of a word.
 * Different child nodes of the same parent node will represent different words with the same prefix
 */
public class Trie {

	/** root is the first node of our Trie, when we'll use the class we'll create and add values into the Trie according to the prefix's first character */
	TrieNode root;


	/**
	 * @param r : TrieNode that will be chosen as the first node in the Trie
	 */
	public Trie(TrieNode r) {
		this.root = r;
	}

	/**
	 * @return if root is null returns true, otherwise false
	 */
	boolean isEmpty() {
		return (this.root == null);
	}

	/**
	 * @param word : the word we want to insert
	 */
	void insert(String word) {
		if (search(word))
			return; // the word is already there we don't have to insert it
		TrieNode current = this.root;
		TrieNode pre;
		for (char ch : word.toCharArray()) {
			pre = current;
			TrieNode child = current.getChild(ch);
			if (child != null) {
				current = child;
				child.parent = pre;
			} else {
				current.children.add(new TrieNode(ch));
				current = current.getChild(ch);
				current.parent = pre;
			}
		}
		current.isEnd = true;
	}

	/**
	 * @param word : the word we are looking for
	 * @return true if the word is in the Trie, otherwise false
	 */
	boolean search(String word) {
		TrieNode current = this.root;
		for (char ch : word.toCharArray()) {
			if (current.getChild(ch) == null)
				return false;
			current = current.getChild(ch);
		}
		if (current.isEnd) {
			return true; // we reached the end of a word, it is in the trie
		}
		return false; // we didn't reach any end of word, the word we are looking for isn't there
	}

	/**
	 * @param pattern pattern we want to match
	 * @return List composed of words matching pattern
	 */
	public List<String> autocomplete(String pattern) {
		TrieNode lastNode = this.root;
		for (int i = 0; i < pattern.length(); i++) {
			lastNode = lastNode.getChild(pattern.charAt(i));
			if (lastNode == null)
				return new ArrayList<>();
		}
		return lastNode.getWords();
	}
}
