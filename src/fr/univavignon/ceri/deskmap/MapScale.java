package fr.univavignon.ceri.deskmap;

class MapScale {

    static double maxWidth = Interface.canvasMap.getWidth();
    static double maxHeight = Interface.canvasMap.getHeight();

    static double scaleInMeters;
    static double scaleWidth = maxWidth / 5;

    static void draw() {
        Interface.gc.setLineWidth(2);
        Interface.gc.setStroke(MapColor.gray);

        // Move to the first point of the map scale
        Interface.gc.moveTo(20, maxHeight - 15);

        // Draw the map scale
        Interface.gc.beginPath();
        Interface.gc.lineTo(20, maxHeight - 15);
        Interface.gc.lineTo(20, maxHeight - 10);
        Interface.gc.lineTo(20 + scaleWidth, maxHeight - 10);
        Interface.gc.lineTo(20 + scaleWidth, maxHeight - 15);

        Interface.gc.stroke();

        // Scale text
        String textValue;
        if (scaleInMeters > 1000) {
            double scaleInKilometers = scaleInMeters / 1000;
            textValue = (Math.round(scaleInKilometers * 10) / 10.0) + " km";
        }
        else {
            textValue = Math.round(scaleInMeters) + " m";
        }

        // Write current scale
        Interface.gc.setFill(MapColor.gray);
        Interface.gc.fillText(textValue, 25 + scaleWidth, maxHeight - 10);
    }
}
