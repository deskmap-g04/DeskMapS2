package fr.univavignon.ceri.deskmap;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

/**
 * @author LA Vivien 
 * @source : https://www.youtube.com/watch?v=wjWSfRfiiIs
 * A Trie is composed of TrieNodes : a TrieNode has a character, a parent TrieNode, a list of its children TrieNodes 
 * and an indicator to tell whether this node has children TrieNodes or not
 */
class TrieNode {

	/** The character contained by the TrieNode we are working on */
	char data;

	/** LinkedList of TrieNodes to contain all the children nodes of the TrieNode we are working on */
	LinkedList<TrieNode> children;

	/** The parent TrieNode of the node we are working on */
	TrieNode parent;

	/**
	 * False if the node has children nodes, true otherwise
	 * If the node doesn't have children nodes, it is the end of a word 
	 */
	boolean isEnd ;
	
	/**
	 * Constructor for TrieNode using a given character as data. It sets the node as not being the end of a word but not having any child
	 * @param c : the given character we want to instance with
	 */
	TrieNode(char c) {
		this.data = c ;
		this.children = new LinkedList<>() ;
		this.isEnd = false ;
	}

	/**
	 * @param c : the character we are looking for among the node's children
	 * @return the child node if there's one with the right data or null if it doesn't exist
	 */
	TrieNode getChild(char c) {
		if(this.children != null) {
			for(TrieNode eachChild : this.children) {
				if(eachChild.data == c || eachChild.data == Character.toUpperCase(c)) {
					return eachChild ;
				}
			}
		}
		return null ;
	}

	/**
	 * @return all the words beginning with the prefix composed of the node and its parent nodes
	 */
	List<String> getWords() {
		List<String> list = new ArrayList<>() ;
		if(this.isEnd) {
			list.add(toString()); 	//if there's a word matching the pattern, we simply add it to the list
		}
		if(this.children != null) { // there are words matching the pattern
			for (TrieNode child : this.children) {
				if (child != null) {
					list.addAll(child.getWords());
				}
			}
		}
		return list ;
	}

	public String toString() {
		char[] tmp = new char[1] ;
		tmp[0] = this.data ;
		if(this.parent == null) return "" ;
		return this.parent.toString() + new String(tmp) ;
	}
}
