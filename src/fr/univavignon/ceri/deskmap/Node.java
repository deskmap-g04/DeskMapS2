package fr.univavignon.ceri.deskmap;

class Node {

    long id;

    double lon;
    double lat;

    double xPos;
    double yPos;

    int recalculatedIndex = 0;


    Node(long id, double lon, double lat) {
        this.id = id;
        this.lon = lon;
        this.lat = lat;
    }

    Node(double lon, double lat){
        // id -1 for bus
        this.id = -1;
        this.lon = lon;
        this.lat = lat;
    }

    double getDistanceInMetersTo(Node B) {
        return DistanceCalculator.getDistanceInMeters(this, B);
    }

    public boolean equals(Node node) {
        return (lat == node.lat && lon == node.lon);
    }
}
