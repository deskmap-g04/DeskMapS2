package fr.univavignon.ceri.deskmap;

import javafx.scene.paint.Color;

class MapColor {
    // Background
    static Color backgroundColor = Color.rgb(242, 239, 233);

    // Shades of gray
    static Color black = Color.rgb(0, 0, 0);
    static Color darkGray = Color.rgb(63, 63, 63);
    static Color gray = Color.rgb(127, 127, 127);
    static Color lightGray = Color.rgb(191, 191, 191);
    static Color white = Color.rgb(255, 255, 255);

    // Crossroad colors
    static Color crossroadColor = Color.rgb(255, 127, 127, 0.5);
    static Color departureColor = Color.rgb(0, 0, 255, 0.5);
    static Color arrivalColor = Color.rgb(255, 0, 0, 0.5);
    static Color itineraryColor = Color.rgb(102, 157, 246);

    // Debug colors
    static Color red = Color.rgb(255, 0, 0);
    static Color green = Color.rgb(0, 255, 0);
    static Color blue = Color.rgb(0, 0, 255);

    // Road colors
    static Color motorwayOutline = Color.rgb(232, 146, 162);
    static Color trunkOutline = Color.rgb(249, 178, 156);
    static Color primaryOutline = Color.rgb(252, 214, 164);
    static Color secondaryOutline = Color.rgb(247, 250, 191);
    static Color tertiaryOutline = Color.rgb(255, 255, 255);
    static Color livingStreetOutline = Color.rgb(220, 220, 220);
    static Color pedestrianOutline = Color.rgb(210, 210, 210);
    static Color trackOutline = Color.rgb(177, 138, 61);
    static Color cyclewayOutline = Color.rgb(23, 23, 254);
    static Color footwayOutline = Color.rgb(250, 134, 121);
    static Color bridlewayOutline = Color.rgb(23, 139, 22);

    // Disabled road colors
    static Color motorwayOutlineDisabled = motorwayOutline.grayscale();
    static Color trunkOutlineDisabled = trunkOutline.grayscale();
    static Color primaryOutlineDisabled = primaryOutline.grayscale();
    static Color secondaryOutlineDisabled = secondaryOutline.grayscale();
    static Color tertiaryOutlineDisabled = tertiaryOutline.grayscale();
    static Color livingStreetOutlineDisabled = livingStreetOutline.grayscale();
    static Color pedestrianOutlineDisabled = pedestrianOutline.grayscale();
    static Color trackOutlineDisabled = trackOutline.grayscale();
    static Color cyclewayOutlineDisabled = cyclewayOutline.grayscale();
    static Color footwayOutlineDisabled = footwayOutline.grayscale();
    static Color bridlewayOutlineDisabled = bridlewayOutline.grayscale();

    // Building colors
    static Color buildingOutline = Color.rgb(201, 185, 175);
    static Color buildingFiller = Color.rgb(217, 208, 201);

    // Water color
    static Color waterFiller = Color.rgb(170, 211, 223);

    // Park color
    static Color parkFiller = Color.rgb(200, 250, 204);

    // Landuse colors
    static Color landuseCommercial = Color.rgb(242, 217, 216);
    static Color landuseConstruction = Color.rgb(199, 199, 180);
    static Color landuseIndustrial = Color.rgb(235, 219, 233);
    static Color landuseResidential = Color.rgb(225, 225, 225);
    static Color landuseRetail = Color.rgb(255, 213, 208);

    static Color landuseAllotments = Color.rgb(202, 226, 192);
    static Color landuseFarmland = Color.rgb(239, 240, 214);
    static Color landuseFarmyard = Color.rgb(234, 204, 164);
    static Color landuseForest = Color.rgb(157, 202, 138);
    static Color landuseMeadow = Color.rgb(206, 236, 177);
    static Color landuseOrchard = Color.rgb(158, 220, 144);
    static Color landuseVineyard = Color.rgb(158, 220, 144);

    static Color landuseBasin = Color.rgb(171, 212, 224);
    static Color landuseBrownfield = Color.rgb(182, 182, 144);
    static Color landuseCemetery = Color.rgb(171, 204, 176);
    static Color landuseGarages = Color.rgb(222, 221, 204);
    static Color landuseGrass = Color.rgb(207, 237, 165);
    static Color landuseGreenfield = Color.rgb(241, 238, 232);
    static Color landuseGreenhouseHorticulture = Color.rgb(239, 240, 214);
    static Color landuseLandfill = Color.rgb(182, 182, 144);
    static Color landuseMilitary = Color.rgb(243, 229, 223);
    static Color landusePlantNursery = Color.rgb(176, 225, 165);
    static Color landuseQuarry = Color.rgb(183, 181, 181);
    static Color landuseRailway = Color.rgb(230, 209, 227);
    static Color landuseRecreationGround = Color.rgb(224, 252, 227);
    static Color landuseReligious = Color.rgb(206, 205, 202);
    static Color landuseReservoir = Color.rgb(171, 212, 224);
    static Color landuseVillageGreen = Color.rgb(206, 236, 177);

    static Color landuseTourism = Color.rgb(222,246 ,192 );
    static Color landuseHealthcare = Color.rgb(255, 255, 229);
}
