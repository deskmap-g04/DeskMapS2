package fr.univavignon.ceri.deskmap;

import javafx.scene.image.Image;

class Icon {
    static int smallSize = 10;
    static int mediumSize = 20;

    static Image bus = new Image("file:icons/bus.png");
    static Image busStop = new Image("file:icons/busStop.png");
    static Image oneWayArrow = new Image("file:icons/oneWayArrow.png");
}
